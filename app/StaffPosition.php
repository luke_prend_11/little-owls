<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffPosition extends Model
{
    /**
     * Get the position record associated with the staff position.
     */
    public function position()
    {
        return $this->belongsTo('App\Position');
    }
    
    /**
     * Get the staff record associated with the staff position.
     */
    public function staff()
    {
        return $this->belongsTo('App\Staff');
    }
}
