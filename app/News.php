<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'contents', 'download_id', 'link_text', 'created_at', 'updated_at'
    ];
    
    /**
     * Get the download record associated with the news item.
     */
    public function download()
    {
        return $this->belongsTo('App\Download');
    }
}
