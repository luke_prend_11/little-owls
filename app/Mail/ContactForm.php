<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    private $name;
    private $email;
    private $enquiry;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $enquiry)
    {
        $this->name = $name;
        $this->email = $email;
        $this->enquiry = $enquiry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Enquiry Form';

        return $this->view('emails.contactForm')
            ->from(Config::get('constants.CONTACT_EMAIL'))
            ->subject($subject)
            ->with('name', $this->name)
            ->with('email', $this->email)
            ->with('enquiry', $this->enquiry);
    }
}
