<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'file_name', 'created_at', 'updated_at'
    ];
    
    /**
     * Get the download record associated with the news item.
     */
    public function news()
    {
        return $this->hasOne('App\News', 'download_id');
    }
}
