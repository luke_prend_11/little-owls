<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    /**
     * Get the staff position record associated with the job position.
     */
    public function staffPosition()
    {
        return $this->hasOne('App\StaffPosition');
    }
}
