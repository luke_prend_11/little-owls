<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolHoliday extends Model
{
    /**
     * Get the holiday name record associated with the school holiday entry.
     */
    public function holiday()
    {
        return $this->belongsTo('App\Holiday');
    }
    
    /**
     * Get the school record associated with the school holiday entry.
     */
    public function school()
    {
        return $this->belongsTo('App\School');
    }
    
    /**
     * Get holidays for year
     * 
     * @param type $query
     * @param type $yearId
     * @return type
     */
    public function scopeCurrentYear($query, $yearId)
    {
        return $query->where([
            ['year_id', $yearId]
        ]);
    }
}
