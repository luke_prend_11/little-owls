<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffQualification extends Model
{
    /**
     * Get the qualification record associated with the staff qualification.
     */
    public function qualification()
    {
        return $this->belongsTo('App\Qualification');
    }
    
    /**
     * Get the staff record associated with the staff qualification.
     */
    public function staff()
    {
        return $this->belongsTo('App\Staff');
    }
}
