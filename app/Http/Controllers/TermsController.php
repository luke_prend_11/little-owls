<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Holiday;
use App\SchoolHoliday;
use App\SchoolYear;

class TermsController extends Controller
{
    private $currentDate;
    private $currentDatePlusOneYear;

    private $currentSchoolYear;
    private $nextSchoolYear;
    private $currentSchoolYearHolidays;
    private $nextSchoolYearHolidays;

    public function showHolidays()
    {
        $this->setCurrentDate();

        // set current school year and next school year
        $this->currentSchoolYear = $this->setSchoolYear($this->currentDate);
        $this->nextSchoolYear    = $this->setSchoolYear($this->currentDatePlusOneYear);

        // save holidays for current school year/next school year to array
        $this->currentSchoolYearHolidays     = $this->currentSchoolYear ? $this->getHolidays($this->currentSchoolYear) : null;
        $this->currentSchoolYearBankHolidays = $this->currentSchoolYear ? $this->getBankHolidays($this->currentSchoolYear) : null;
        $this->nextSchoolYearHolidays        = $this->nextSchoolYear ? $this->getHolidays($this->nextSchoolYear) : null;
        $this->nextSchoolYearBankHolidays    = $this->nextSchoolYear ? $this->getBankHolidays($this->nextSchoolYear) : null;

        return view('pages.about.term-dates')
                ->with('currentSchoolYear', $this->currentSchoolYear)
                ->with('nextSchoolYear', $this->nextSchoolYear)
                ->with('currentSchoolYearHolidays', $this->currentSchoolYearHolidays)
                ->with('currentSchoolYearBankHolidays', $this->currentSchoolYearBankHolidays)
                ->with('nextSchoolYearHolidays', $this->nextSchoolYearHolidays)
                ->with('nextSchoolYearBankHolidays', $this->nextSchoolYearBankHolidays);
    }

    private function setCurrentDate()
    {
        date_default_timezone_set('Europe/London');
        $this->currentDate            = date('Y-m-d');
        $this->currentDatePlusOneYear = date('Y-m-d', strtotime('+1 year'));
    }

    private function setSchoolYear($date)
    {
        return SchoolYear::where('closes', '>=', $date)
                ->orderBy('opens', 'ASC')
                ->first();
    }

    private function getHolidays($schoolYearDate)
    {
        return Holiday::with(['school_holiday' => function($query) use($schoolYearDate) {
                $query->currentYear($schoolYearDate->id);
                $query->with('school');
            }])
            ->where('order', '<>', 0)
            ->orderBy('order', 'ASC')
            ->get();
    }

    private function getBankHolidays($schoolYearDate)
    {
        return SchoolHoliday::with(['holiday' => function($query) {
                $query->where('display', 1);
            }])
            ->whereColumn('closes', 're_opens')
            ->currentYear($schoolYearDate->id)
            ->orderBy('closes', 'ASC')
            ->get();
    }

    public function getNextTwoHolidays()
    {
        $this->setCurrentDate();

        // get next closing date
        $nextReOpensDate = SchoolHoliday::where('re_opens', '>', $this->currentDate)
                ->whereColumn('closes', '<>', 're_opens')
                ->min('re_opens');

        if($nextReOpensDate) {
            $nextHoliday = $this->getHoliday($nextReOpensDate);

            // get next next closing date
            $nextNextReOpensDate = SchoolHoliday::where('re_opens', '>', $nextReOpensDate)
                    ->whereColumn('closes', '<>', 're_opens')
                    ->min('re_opens');

            $nextNextHoliday = $this->getHoliday($nextNextReOpensDate);

            return array($nextHoliday, $nextNextHoliday);
        }

        return array(null, null);
    }

    public function getHoliday($reOpenDate)
    {
        return SchoolHoliday::with('holiday')
                ->where('re_opens', $reOpenDate)
                ->whereColumn('closes', '<>', 're_opens')
                ->get();
    }
}
