<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\NewsController;

class HomeController extends Controller
{
    private $homePageNews;
    private $homePageHolidays;

    public function showHomePage()
    {
        $this->setHomePageNews();
        $this->setHomePageHolidays();

        return view('pages.home')
                ->with('homePageNews', $this->homePageNews)
                ->with('nextHoliday', $this->nextHoliday)
                ->with('nextNextHoliday', $this->nextNextHoliday);
    }

    private function setHomePageNews()
    {
        $this->homePageNews = (new NewsController)->getNews(3);
    }

    private function setHomePageHolidays()
    {
        list($this->nextHoliday, $this->nextNextHoliday) = (new TermsController)->getNextTwoHolidays();
    }
}
