<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use App\Qualification;
use App\Position;

class StaffController extends Controller
{
    private $staffMembers;
    
    public function showStaff()
    {
        $this->getStaffMembers();
        
        return view('pages.staff.all')->with('allStaff', $this->staffMembers);
    }
    
    private function getStaffMembers()
    {
        $this->staffMembers = Staff::with('staffQualifications', 'staffQualifications.Qualification', 'staffPosition', 'staffPosition.Position')
                ->orderBy('name', 'ASC')
                ->get();
    }
    
    public function getStaffId($name)
    {
        $staffDetails = Staff::where('name', $name)->first();
        return $staffDetails->id;
    }
    
    public function getQualificationId($qualification)
    {
        $qualificationDetails = Qualification::where('name', $qualification)->first();
        return $qualificationDetails->id;
    }
    
    public function getStaffPositionId($position)
    {
        $positionDetails = Position::where('name', $position)->first();
        return $positionDetails->id;
    }
}
