<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class MetaContent extends Controller
{
    const DEFAULT_META_KEYWORDS    = "nursery, childcare, child care, little owls, little owls nursery, nursery scunthorpe, childcare scunthorpe, child care scunthorpe";
    const DEFAULT_META_DESCRIPTION = "";
    const STYLESHEET_LINK          = '<link rel="stylesheet/less" type="text/css" href="%s/css/%s" />';
    const JAVASCRIPT_LINK          = '<script type="text/javascript" src="%s"></script>';
    const HEADER_IMAGE_LINK        = "images/header-images/%s.jpg";

    public static $baseUrl;
    public static $metaContent;
    public static $currentPageRoute;
    public static $pageTitle;
    public static $metaDescription;
    public static $metaKeywords;
    public static $pageH1;
    public static $css;
    public static $js;
    public static $headerImage;
    public static $headingsLinks;
    
    public static function setMetaData()
    {
        self::setBaseUrl();
        self::setMetaContent();
        self::setCurrentPageRoute();
        self::$pageTitle = self::setPageTitle();
        self::setMetaDescription();
        self::setMetaKeywords();
        self::setPageH1();
        self::setCss();
        self::setJs();
        self::setHeaderImage();
        self::setHeadingsLinks();
    }
    
    public static function setBaseUrl()
    {
        self::$baseUrl = URL::to('/');
    }
    
    public static function setMetaContent()
    {
        self::$metaContent = array(
            "_" => array(
                "page_title"       => config('constants.SITE_NAME')." | Child Care Scunthorpe",
                "meta_description" => "Welcome to ".config('constants.SITE_NAME').". At Little Owls we provide a high quality childcare and pre-school education.",
                "css"              => "home.less",
                "h1"               => config('constants.SITE_NAME'),
            ),
            "contact_us" => array(
                "page_title"       => "Contact ".config('constants.SHORT_SITE_NAME'),
                "meta_description" => "For any enquiries regarding ".config('constants.SITE_NAME').", please use our contact form.",
                "css"              => "contact.less",
            ),
            "newsletters" => array(
                "page_title"       => "News &amp; Bulletins",
                "meta_description" => "Find the latest news and bulletins for ".config('constants.SITE_NAME').".",
                "css"              => "news.less",
            ),
            "ofsted" => array(
                "page_title"       => "Ofsted Reports",
                "meta_description" => "View the latest ofsted report for ".config('constants.SITE_NAME').".",
                "css"              => "ofsted.less",
            ),
            "data_protection" => array(
                "page_title"       => "Data Protection Policy",
                "meta_description" => config('constants.SITE_NAME')." General Data Protection Regulation (GDPR) compliance.",
            ),
            "about_eyfs" => array(
                "page_title"       => "Early Years Foundation Stage",
                "meta_description" => "More information on EYFS including key areas of learning and development.",
            ),
            "about_parental_essentials" => array(
                "page_title"       => "Information for Parents",
                "meta_description" => "Find important information regarding your child and their attendance at ".config('constants.SITE_NAME'),
                "headings_links"   => TRUE,
                "h1"               => "Parental Essentials"
            ),
            "about_term_dates" => array(
                "page_title"       => "Nursery Term Dates",
                "meta_description" => "Find term opening and closing dates for ".config('constants.SITE_NAME').".",
                "css"              => "term-dates.less",
                "headings_links"   => TRUE,
            ),
            "staff" => array(
                "page_title"       => "Our Staff",
                "meta_description" => "Meet the staff at ".config('constants.SITE_NAME').", Scunthorpe.",
                "css"              => "staff.less",
                "headings_links"   => TRUE,
            ),
            "staff_keyperson" => array(
                "page_title"       => "Key Person Policy",
                "meta_description" => "Key Person policy for ".config('constants.SITE_NAME').", Scunthorpe.",
                "headings_links"   => TRUE,
            ),
            "staff_training" => array(
                "page_title"       => "Staff Training",
                "meta_description" => "Staff training at ".config('constants.SITE_NAME').", Scunthorpe.",
                "headings_links"   => TRUE,
            ),
            "curriculum" => array(
                "page_title"       => "Curriculum",
                "meta_description" => "More information on the curriculum followed by ".config('constants.SITE_NAME').".",
            ),
            "curriculum_personal_social_emotional" => array(
                "page_title"       => "Personal, Social &amp; Emotional Development",
                "meta_description" => "Personal, Social and Emotional Development at ".config('constants.SITE_NAME').".",
            ),
            "curriculum_communication_language" => array(
                "page_title"       => "Communication &amp; Language Development",
                "meta_description" => "Communication &amp; Language Development at ".config('constants.SITE_NAME').".",
            ),
            "curriculum_physical_development" => array(
                "page_title"       => "Physical Development",
                "meta_description" => "Discover what's involved with physical development at ".config('constants.SITE_NAME').".",
            ),
            "curriculum_literacy" => array(
                "page_title"       => "Literacy",
                "meta_description" => "Literacy Development at ".config('constants.SITE_NAME').".",
                "headings_links"   => TRUE,
            ),
            "curriculum_mathematics" => array(
                "page_title"       => "Mathematics",
                "meta_description" => "Mathematics &amp; Numeracy Development at ".config('constants.SITE_NAME').".",
            ),
            "curriculum_the_world" => array(
                "page_title"       => "Understanding the World",
                "meta_description" => "Developing an understanding of the world at ".config('constants.SITE_NAME').".",
            ),
            "curriculum_expressive_arts" => array(
                "page_title"       => "Expressive Arts &amp; Design",
                "meta_description" => "Helping children to explore their creative skills at ".config('constants.SITE_NAME').".",
            ),
            "policies" => array(
                "page_title"       => "Policies",
                "meta_description" => "More information on the policies followed by ".config('constants.SITE_NAME').".",
            ),
            "policies_child_protection" => array(
                "page_title"       => "Child Protection Policy",
                "meta_description" => "More information on ".config('constants.SHORT_SITE_NAME')." Child Protection Policy.",
                "headings_links"   => TRUE,
            ),
            "policies_confidentiality" => array(
                "page_title"       => "Confidentiality Policy",
                "meta_description" => "More information on the ".config('constants.SHORT_SITE_NAME')." Confidentiality Policy.",
            ),
            "policies_equal_opportunities" => array(
                "page_title"       => "Equality of Opportunities Policy",
                "meta_description" => "Information on the ".config('constants.SHORT_SITE_NAME')." Equality of Opportunities Policy.",
                "headings_links"   => TRUE,
            ),
            "policies_no_smoking" => array(
                "page_title"       => "No Smoking Policy",
                "meta_description" => "Information on the ".config('constants.SHORT_SITE_NAME')." No Smoking Policy.",
            ),
            "policies_mobile_phone" => array(
                "page_title"       => "Mobile Phone and Social Networking Policy",
                "meta_description" => "More information on ".config('constants.SHORT_SITE_NAME')." Mobile Phone and Social Networking Policy.",
                "headings_links"   => TRUE,
            ),
            "policies_complaints" => array(
                "page_title"       => "Complaints Policy",
                "meta_description" => "More information on ".config('constants.SHORT_SITE_NAME')." Complaints Policy.",
                "headings_links"   => TRUE,
            ),
            "policies_admissions" => array(
                "page_title"       => "Admissions Policy",
                "meta_description" => "More information on ".config('constants.SHORT_SITE_NAME')." Admissions Policy.",
                "headings_links"   => TRUE,
            ),
            "policies_behaviour_management" => array(
                "page_title"       => "Promoting Positive Behaviour",
                "meta_description" => "At ".config('constants.SHORT_SITE_NAME')." we believe that children flourish best when they know how they and others are expected to behave.",
                "headings_links"   => TRUE,
            ),
            "policies_late_collection" => array(
                "page_title"       => "Late Collection Policy",
                "meta_description" => "More information on ".config('constants.SHORT_SITE_NAME')." Late Collection Policy.",
            ),
            "policies_fees_and_funding" => array(
                "page_title"       => "Fees and Funding Policy",
                "meta_description" => "More information on ".config('constants.SHORT_SITE_NAME')." Fees and Funding Policy.",
                "headings_links"   => TRUE,
                "css" => "staff.less"
            ),
            "policies_medication" => array(
                "page_title"       => "Medication Policy",
                "meta_description" => "More information on ".config('constants.SHORT_SITE_NAME')." Medication Policy.",
                "headings_links"   => TRUE,
            ),
            "policies_intimate_care" => array(
                "page_title"       => "Intimate Care Policy",
                "meta_description" => "More information on ".config('constants.SHORT_SITE_NAME')." Intimate Care Policy.",
            ),
            "policies_transition" => array(
                "page_title"       => "Transition Policy",
                "meta_description" => "More information on ".config('constants.SHORT_SITE_NAME')." Transition Policy.",
                "headings_links"   => TRUE,
            ),
            "policies_settling_in" => array(
                "page_title"       => "Settling in Policy",
                "meta_description" => "More information on ".config('constants.SHORT_SITE_NAME')." Settling in Policy.",
            ),
            "policies_healthy_eating" => array(
                "page_title"       => "Nutrition and snack times",
                "meta_description" => "At ".config('constants.SHORT_SITE_NAME')." we believe that mealtimes should be happy, social occasions for children and staff alike. We promote shared, enjoyable positive interactions at these times.",
            ),
            "login" => array(
                "page_title"       => "Admin Login",
            ),
            "admin" => array(
                "page_title"       => "Admin Home",
            ),
            "admin_news" => array(
                "page_title"       => "Admin News",
            ),
            "404" => array(
                "page_title"       => "Page Not Found",
                "meta_description" => "The page you are trying to visit does not exist. Please contact us for more information.",
            )
        );
    }
    
    public static function setCurrentPageRoute()
    {
        self::$currentPageRoute = Route::getCurrentRoute() !== null ? self::removeSpecialCharacters() : "404";
    }
    
    public static function removeSpecialCharacters()
    {
        $toReplace   = array("/","-");
        $replaceWith = array("_","_");
        
        return str_replace($toReplace, $replaceWith, \Request::path());
    }
    
    public static function setPageTitle()
    {
        if(isset(self::$metaContent[self::$currentPageRoute]["page_title"]) && self::$metaContent[self::$currentPageRoute]["page_title"] != "")
        {
            return stripos(self::$metaContent[self::$currentPageRoute]["page_title"], config('constants.SITE_NAME')) !== false
                ? self::$metaContent[self::$currentPageRoute]["page_title"]
                : self::$metaContent[self::$currentPageRoute]["page_title"].config('constants.PAGE_TITLE_SEPARATOR').config('constants.SITE_NAME');
        }
        return config('constants.SITE_NAME');
    }

    public static function setMetaDescription()
    {
        self::$metaDescription = isset(self::$metaContent[self::$currentPageRoute]["meta_description"]) && self::$metaContent[self::$currentPageRoute]["meta_description"] != ""
                               ? self::$metaContent[self::$currentPageRoute]["meta_description"] 
                               : self::DEFAULT_META_DESCRIPTION;
    }

    public static function setMetaKeywords()
    {
        self::$metaKeywords = isset(self::$metaContent[self::$currentPageRoute]["meta_keywords"]) && self::$metaContent[self::$currentPageRoute]["meta_keywords"] != ""
                            ? self::$metaContent[self::$currentPageRoute]["meta_keywords"] 
                            : self::DEFAULT_META_KEYWORDS;
    }

    public static function setPageH1()
    {
        self::$pageH1 = isset(self::$metaContent[self::$currentPageRoute]["h1"]) 
                      ? self::$metaContent[self::$currentPageRoute]["h1"] 
                      : self::$metaContent[self::$currentPageRoute]["page_title"];
    }

    public static function setCss()
    {
        if(isset(self::$metaContent[self::$currentPageRoute]["css"]))
        {
            self::$css = self::buildAsset(self::$metaContent[self::$currentPageRoute]["css"]);
        }
    }

    public static function setJs()
    {
        if(isset(self::$metaContent[self::$currentPageRoute]["js"]))
        {
            self::$js = self::buildAsset(self::$metaContent[self::$currentPageRoute]["js"]);
        }
    }

    public static function buildAsset($asset)
    {
        if(is_array($asset))
        {
            $string = "";
            foreach ($asset as $assetLink)
            {
                $string .= self::createAssetLink($assetLink);
                $string .= "\n";
            }
        }
        else
        {
            $string = self::createAssetLink($asset);
            $string .= "\n";
        }
        return $string;
    }

    public static function createAssetLink($asset)
    {
        return strpos($asset, 'css') || strpos($asset, 'less') !== false 
            ? sprintf(self::STYLESHEET_LINK, self::$baseUrl, $asset) 
            : sprintf(self::JAVASCRIPT_LINK, $asset);
    }

    public static function setHeaderImage()
    {
        if(isset(self::$metaContent[self::$currentPageRoute]["header_image"]))
        {
            self::buildHeaderImage(self::$metaContent[self::$currentPageRoute]["header_image"]);
        }
    }

    public static function setHeadingsLinks()
    {
        self::$headingsLinks = isset(self::$metaContent[self::$currentPageRoute]["headings_links"]) ? TRUE : FALSE;
    }

    public static function buildHeaderImage($headerImage)
    {
        self::$headerImage = '<div id="home-header" style="background-image: url(\''.self::createHeaderImageLink($headerImage['file_name']).'\');" class="row parallax-image">';
        
        if(isset($headerImage['image_caption']) || isset($headerImage['button_link']))
        {
            self::$headerImage .= '<div class="content">';
            self::$headerImage .= isset($headerImage['image_caption']) ? '<span class="slogan">'.$headerImage['image_caption'].'</span>' : '';
            self::$headerImage .= isset($headerImage['button_link']) ? '<a class="button" href="'.$headerImage['button_link'].'" title="'.$headerImage['button_link_title'].'" target="_blank">' : '';
            self::$headerImage .= isset($headerImage['button_link']) ? $headerImage['button_text'] : '';
            self::$headerImage .= isset($headerImage['button_link']) ? '</a>' : '';
            self::$headerImage .= '</div>';
        }
        
        self::$headerImage .= '</div>';
    }

    public static function createHeaderImageLink($image)
    {
        //return sprintf(self::HEADER_IMAGE_LINK, $image);
        return URL::asset('images/header-images/'.$image.'.jpg');
    }
}
