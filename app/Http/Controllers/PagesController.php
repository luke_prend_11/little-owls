<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    
    public function getPage($page)
    {
        $pageDetails = Page::where('page', $page)->first();

        return view('dynamic-page', compact('pageDetails'));
    }
}
