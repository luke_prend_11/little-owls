<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{    
    public function showNews()
    {
        // calculate number of posts to limit and skip for each column of news
        $allNews      = News::all();
        $allNewsCount = count($allNews);
        $latestCount  = ceil($allNewsCount / 2);
        $olderCount   = $allNewsCount - $latestCount;
        
        $latestNews = $this->getNews($latestCount);
        $olderNews  = $this->getNews($olderCount, $latestCount);
                
        return view('pages.news')
                ->with('latestNews', $latestNews)
                ->with('olderNews', $olderNews);
    }
    
    public function getNews($limit = 0, $skip = 0)
    {
        return News::with('download')
                ->orderBy('updated_at', 'DESC')
                ->skip($skip)
                ->limit($limit)
                ->get();
    }
}
