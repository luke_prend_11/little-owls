<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Mail\ContactForm;

class ContactController extends Controller
{
    const SUCCESS_MESSAGE = "Thank you. Your enquiry has been sent. We aim to respond to all enquiries within 48 hours.";
    
    public function submit(Request $request)
    {
        // validate fields
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'comments' => 'required|string|min:10|max:1000',
        ]);
        
        // send email
        $this->sendEmail($request->get('name'), $request->get('email'), $request->get('comments'));
        
        // return to contact view with success message
        return view('pages.contact')->with('success', self::SUCCESS_MESSAGE);
    }
    
    private function sendEmail($name, $email, $comments)
    {
        \Mail::to(Config::get('constants.CONTACT_EMAIL'))
                ->send(new ContactForm($name, $email, $comments));
    }
}
