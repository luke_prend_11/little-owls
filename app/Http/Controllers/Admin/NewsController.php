<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    private $allNews;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getNews();
        return view('admin.news')->with('news', $this->allNews);
    }
    
    /**
     * Get news from model
     * 
     * @return void
     */
    private function getNews()
    {
        $this->allNews = News::all();
    }
}
