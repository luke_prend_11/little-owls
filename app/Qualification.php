<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    /**
     * Get the staff qualification record associated with the qualification record.
     */
    public function staffQualification()
    {
        return $this->hasOne('App\StaffQualification');
    }
}
