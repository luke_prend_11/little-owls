<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    /**
     * Get the staff position record associated with the staff record.
     */
    public function staffPosition()
    {
        return $this->hasOne('App\StaffPosition');
    }
    
    /**
     * Get the staff qualification record associated with the staff record.
     */
    public function staffQualifications()
    {
        return $this->hasMany('App\StaffQualification');
    }
    
    /**
     * Get the user record associated with the staff record.
     */
    public function user()
    {
        return $this->hasOne('App\User');
    }
}
