<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    /**
     * Get the holiday name record associated with the school holiday entry.
     */
    public function school_holiday()
    {
        return $this->hasMany('App\SchoolHoliday');
    }
}
