<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@showHomePage')->name('home');
Route::get('contact-us', function () { 
    return View::make('pages.contact');
})->name('contact');
Route::post('contact-us', 'ContactController@submit')->name('contact-form-submit');

Route::get('about/eyfs', function () { 
    return View::make('pages.about.eyfs');
})->name('eyfs');

Route::get('about/parental-essentials', function () { 
    return View::make('pages.about.parental-essentials');
})->name('parental-essentials');

Route::get('about/term-dates', 'TermsController@showHolidays')->name('term-dates');

Route::get('newsletters', 'NewsController@showNews')->name('newsletters');

Route::get('ofsted', function () { 
    return View::make('pages.ofsted');
})->name('ofsted');

Route::get('data-protection', function () { 
    return View::make('pages.data-protection');
})->name('data-protection');

Route::get('staff/', 'StaffController@showStaff')->name('staff');
Route::get('staff/training', function () { 
    return View::make('pages.staff.training');
})->name('staff-training');
Route::get('staff/keyperson', function () { 
    return View::make('pages.staff.keyperson');
})->name('staff-keyperson');

//Route::get('{curriculum}', 'PagesController@getPage');

Route::get('curriculum', function () { 
    return View::make('pages.curriculum.all');
})->name('curriculum');
Route::get('curriculum/personal-social-emotional', function () { 
    return View::make('pages.curriculum.personal');
})->name('curriculum-personal');
Route::get('curriculum/communication-language', function () { 
    return View::make('pages.curriculum.communication');
})->name('curriculum-communication');
Route::get('curriculum/physical-development', function () { 
    return View::make('pages.curriculum.pd');
})->name('curriculum-pd');
Route::get('curriculum/literacy', function () { 
    return View::make('pages.curriculum.literacy');
})->name('curriculum-literacy');
Route::get('curriculum/mathematics', function () { 
    return View::make('pages.curriculum.maths');
})->name('curriculum-maths');
Route::get('curriculum/the-world', function () { 
    return View::make('pages.curriculum.world');
})->name('curriculum-world');
Route::get('curriculum/expressive-arts', function () { 
    return View::make('pages.curriculum.ea');
})->name('curriculum-ea');


Route::get('policies', function () { 
    return View::make('pages.policies.all');
})->name('policies');
Route::get('policies/child-protection', function () { 
    return View::make('pages.policies.child-protection');
})->name('policies-child-protection');
Route::get('policies/confidentiality', function () { 
    return View::make('pages.policies.confidentiality');
})->name('policies-confidentiality');
Route::get('policies/equal-opportunities', function () { 
    return View::make('pages.policies.equal-opportunities');
})->name('policies-equal-opportunities');
Route::get('policies/no-smoking', function () { 
    return View::make('pages.policies.smoking');
})->name('policies-smoking');
Route::get('policies/mobile-phone', function () { 
    return View::make('pages.policies.mobile-phone');
})->name('policies-mobile-phone');
Route::get('policies/complaints', function () { 
    return View::make('pages.policies.complaints');
})->name('policies-complaints');
Route::get('policies/admissions', function () { 
    return View::make('pages.policies.admissions');
})->name('policies-admissions');
Route::get('policies/behaviour-management', function () { 
    return View::make('pages.policies.behaviour');
})->name('policies-behaviour');
Route::get('policies/late-collection', function () { 
    return View::make('pages.policies.late-collection');
})->name('policies-late-collection');
Route::get('policies/fees-and-funding', function () { 
    return View::make('pages.policies.fees-and-funding');
})->name('policies-fees-and-funding');
Route::get('policies/medication', function () { 
    return View::make('pages.policies.medication');
})->name('policies-medication');
Route::get('policies/intimate-care', function () { 
    return View::make('pages.policies.intimate-care');
})->name('policies-intimate-care');
Route::get('policies/transition', function () { 
    return View::make('pages.policies.transition');
})->name('policies-transition');
Route::get('policies/settling-in', function () { 
    return View::make('pages.policies.settling-in');
})->name('policies-settling-in');
Route::get('policies/healthy-eating', function () { 
    return View::make('pages.policies.healthy-eating');
})->name('policies-healthy-eating');

Auth::routes();

// 301 redirects
Route::get('term-dates.html', function(){ 
    return redirect(null, 301)->route('term-dates');
});
Route::get('term-dates', function(){ 
    return redirect(null, 301)->route('term-dates');
});
Route::get('about-us.html', function(){ 
    return redirect(null, 301)->route('eyfs');
});
Route::get('comments-submitted.php', function(){ 
    return redirect(null, 301)->route('contact-form-submit');
});
Route::get('contact-us.html', function(){ 
    return redirect(null, 301)->route('contact');
});
Route::get('curriculum/communication-language.html', function(){ 
    return redirect(null, 301)->route('curriculum-communication');
});
Route::get('curriculum/expressive-arts.html', function(){ 
    return redirect(null, 301)->route('curriculum-ea');
});
Route::get('curriculum/index.html', function(){ 
    return redirect(null, 301)->route('curriculum');
});
Route::get('curriculum/literacy.html', function(){ 
    return redirect(null, 301)->route('curriculum-literacy');
});
Route::get('curriculum/mathematics.html', function(){ 
    return redirect(null, 301)->route('curriculum-maths');
});
Route::get('curriculum/personal-social-emotional.html', function(){ 
    return redirect(null, 301)->route('curriculum-personal');
});
Route::get('curriculum/physical-development.html', function(){ 
    return redirect(null, 301)->route('curriculum-pd');
});
Route::get('curriculum/the-world.html', function(){ 
    return redirect(null, 301)->route('curriculum-world');
});
Route::get('index.html', function(){ 
    return redirect(null, 301)->route('home');
});
Route::get('newsletters.html', function(){ 
    return redirect(null, 301)->route('newsletters');
});
Route::get('ofsted.html', function(){ 
    return redirect(null, 301)->route('ofsted');
});
Route::get('policies/admissions.html', function(){ 
    return redirect(null, 301)->route('policies-admissions');
});
Route::get('policies/behaviour-management.html', function(){ 
    return redirect(null, 301)->route('policies-behaviour');
});
Route::get('policies/child-safeguarding.html', function(){ 
    return redirect(null, 301)->route('policies-child-protection');
});
Route::get('policies/complaints.html', function(){ 
    return redirect(null, 301)->route('policies-complaints');
});
Route::get('policies/confidentiality.html', function(){ 
    return redirect(null, 301)->route('policies-confidentiality');
});
Route::get('policies/equal-opportunities.html', function(){ 
    return redirect(null, 301)->route('policies-equal-opportunities');
});
Route::get('policies/fees-and-funding.html', function(){ 
    return redirect(null, 301)->route('policies-fees-and-funding');
});
Route::get('policies/healthy-eating.html', function(){ 
    return redirect(null, 301)->route('policies-healthy-eating');
});
Route::get('policies/index.html', function(){ 
    return redirect(null, 301)->route('policies');
});
Route::get('policies/intimate-care.html', function(){ 
    return redirect(null, 301)->route('policies-intimate-care');
});
Route::get('policies/late-collection.html', function(){ 
    return redirect(null, 301)->route('policies-late-collection');
});
Route::get('policies/medication.html', function(){ 
    return redirect(null, 301)->route('policies-medication');
});
Route::get('policies/mobile-phone.html', function(){ 
    return redirect(null, 301)->route('policies-mobile-phone');
});
Route::get('policies/no-smoking.html', function(){ 
    return redirect(null, 301)->route('policies-smoking');
});
Route::get('policies/settling-in.html', function(){ 
    return redirect(null, 301)->route('policies-settling-in');
});
Route::get('policies/transition.html', function(){ 
    return redirect(null, 301)->route('policies-transition');
});
Route::get('sitemap.html', function(){ 
    return redirect(null, 301)->route('home');
});
Route::get('staff/index.html', function(){ 
    return redirect(null, 301)->route('staff');
});
Route::get('staff/keyperson.html', function(){ 
    return redirect(null, 301)->route('staff-keyperson');
});
Route::get('staff/training.html', function(){ 
    return redirect(null, 301)->route('staff-training');
});
