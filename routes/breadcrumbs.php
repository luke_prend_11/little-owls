<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

Breadcrumbs::register('eyfs', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('eyfs'));
});

Breadcrumbs::register('parental-essentials', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('parental-essentials'));
});

Breadcrumbs::register('term-dates', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('term-dates'));
});

Breadcrumbs::register('data-protection', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('data-protection'));
});

Breadcrumbs::register('contact', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('contact'));
});

Breadcrumbs::register('contact-form-submit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('contact-form-submit'));
});

Breadcrumbs::register('newsletters', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('newsletters'));
});

Breadcrumbs::register('ofsted', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('ofsted'));
});

Breadcrumbs::register('staff', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('staff'));
});

Breadcrumbs::register('staff-training', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('staff-training'));
});

Breadcrumbs::register('staff-keyperson', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(Meta::$pageH1, route('staff-keyperson'));
});

// Curriculum breadcrumbs
Breadcrumbs::register('curriculum', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Curriculum', route('curriculum'));
});

Breadcrumbs::register('curriculum-personal', function ($breadcrumbs) {
    $breadcrumbs->parent('curriculum');
    $breadcrumbs->push(Meta::$pageH1, route('curriculum-personal'));
});

Breadcrumbs::register('curriculum-communication', function ($breadcrumbs) {
    $breadcrumbs->parent('curriculum');
    $breadcrumbs->push(Meta::$pageH1, route('curriculum-communication'));
});

Breadcrumbs::register('curriculum-pd', function ($breadcrumbs) {
    $breadcrumbs->parent('curriculum');
    $breadcrumbs->push(Meta::$pageH1, route('curriculum-pd'));
});

Breadcrumbs::register('curriculum-literacy', function ($breadcrumbs) {
    $breadcrumbs->parent('curriculum');
    $breadcrumbs->push(Meta::$pageH1, route('curriculum-literacy'));
});

Breadcrumbs::register('curriculum-maths', function ($breadcrumbs) {
    $breadcrumbs->parent('curriculum');
    $breadcrumbs->push(Meta::$pageH1, route('curriculum-maths'));
});

Breadcrumbs::register('curriculum-world', function ($breadcrumbs) {
    $breadcrumbs->parent('curriculum');
    $breadcrumbs->push(Meta::$pageH1, route('curriculum-world'));
});

Breadcrumbs::register('curriculum-ea', function ($breadcrumbs) {
    $breadcrumbs->parent('curriculum');
    $breadcrumbs->push(Meta::$pageH1, route('curriculum-ea'));
});

// Policies breadcrumbs
Breadcrumbs::register('policies', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Policies', route('policies'));
});

Breadcrumbs::register('policies-child-protection', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-child-protection'));
});

Breadcrumbs::register('policies-confidentiality', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-confidentiality'));
});

Breadcrumbs::register('policies-equal-opportunities', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-equal-opportunities'));
});

Breadcrumbs::register('policies-smoking', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-smoking'));
});

Breadcrumbs::register('policies-mobile-phone', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-mobile-phone'));
});

Breadcrumbs::register('policies-complaints', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-complaints'));
});

Breadcrumbs::register('policies-admissions', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-admissions'));
});

Breadcrumbs::register('policies-behaviour', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-behaviour'));
});

Breadcrumbs::register('policies-late-collection', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-late-collection'));
});

Breadcrumbs::register('policies-fees-and-funding', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-fees-and-funding'));
});

Breadcrumbs::register('policies-medication', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-medication'));
});

Breadcrumbs::register('policies-intimate-care', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-intimate-care'));
});

Breadcrumbs::register('policies-transition', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-transition'));
});

Breadcrumbs::register('policies-settling-in', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-settling-in'));
});

Breadcrumbs::register('policies-healthy-eating', function ($breadcrumbs) {
    $breadcrumbs->parent('policies');
    $breadcrumbs->push(Meta::$pageH1, route('policies-healthy-eating'));
});

/* Examples
// Home > Blog
Breadcrumbs::register('blog', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Blog', route('blog'));
});

// Home > Blog > [Category]
Breadcrumbs::register('category', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('blog');
    $breadcrumbs->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Post]
Breadcrumbs::register('post', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('category', $post->category);
    $breadcrumbs->push($post->title, route('post', $post));
});
*/