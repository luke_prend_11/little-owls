
        <div id="links">        
            <div class="col-xs-12 col-md-3">
                <h3>Useful Links</h3>
                <ul>
                    <li><a href="{{ route('home') }}" title="Home">Home</a></li>
                    <li><a href="{{ route('staff') }}" title="Our Staff">Our Staff</a></li>
                    <li><a href="{{ route('ofsted') }}" title="Ofsted Reports">Ofsted Reports</a></li>
                    <li><a href="{{ route('term-dates') }}" title="Term Dates">Term Dates</a></li>
                    <li><a href="{{ route('newsletters') }}" title="Newsletters">Newsletters</a></li>
                    <li><a href="{{ route('contact') }}" title="Contact Us">Contact Us</a></li>
                    <li><a href="{{ route('data-protection') }}" title="GDPR Policy">Data Protection Policy</a></li>
                </ul>
            </div>

            <div class="col-xs-12 col-md-9">
                <h3>Our Locations</h3>
                <p>We have 3 locations in Scunthorpe:</p>
                
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <h4>
                            <a href="{{ route('contact') }}#avenue-vivian" title="{{ Config::get('constants.CONTACT_ADDRESS_ROAD_1') }}">
                                {{ Config::get('constants.CONTACT_ADDRESS_ROAD_1') }}
                            </a>
                            <span>(North)</span>
                        </h4>
                        <ul>
                            <li>
                                <i class="fas fa-phone"></i>
                                <a href="tel:{{ Config::get('constants.CONTACT_PHONE_1') }}" title="Call {{ Config::get('constants.SITE_NAME') }}">
                                    {{ Config::get('constants.CONTACT_PHONE_1') }}
                                </a>
                            </li>
                            <li>
                                <i class="fas fa-envelope"></i>
                                <a href="mailto:{{ Config::get('constants.CONTACT_EMAIL_1') }}" title="Email {{ Config::get('constants.SITE_NAME') }}">
                                    {{ Config::get('constants.CONTACT_EMAIL_1') }}
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-12 col-md-4">
                        <h4>
                            <a href="{{ route('contact') }}#dragonby-road" title="{{ Config::get('constants.CONTACT_ADDRESS_ROAD_2') }}">
                                {{ Config::get('constants.CONTACT_ADDRESS_ROAD_2') }}
                            </a>
                            <span>(South)</span>
                        </h4>
                        <ul>
                            <li>
                                <i class="fas fa-phone"></i>
                                <a href="tel:{{ Config::get('constants.CONTACT_PHONE_2') }}" title="Call {{ Config::get('constants.SITE_NAME') }}">
                                    {{ Config::get('constants.CONTACT_PHONE_2') }}
                                </a>
                            </li>
                            <li>
                                <i class="fas fa-envelope"></i>
                                <a href="mailto:{{ Config::get('constants.CONTACT_EMAIL_2') }}" title="Email {{ Config::get('constants.SITE_NAME') }}">
                                    {{ Config::get('constants.CONTACT_EMAIL_2') }}
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-12 col-md-4">
                        <h4>
                            <a href="{{ route('contact') }}#cowper-avenue" title="{{ Config::get('constants.CONTACT_ADDRESS_ROAD_3') }}">
                                {{ Config::get('constants.CONTACT_ADDRESS_ROAD_3') }}
                            </a>
                            <span>(West)</span>
                        </h4>
                        <ul>
                            <li>
                                <i class="fas fa-phone"></i>
                                <a href="tel:{{ Config::get('constants.CONTACT_PHONE_3') }}" title="Call {{ Config::get('constants.SITE_NAME') }}">
                                    {{ Config::get('constants.CONTACT_PHONE_3') }}
                                </a>
                            </li>
                            <li>
                                <i class="fas fa-envelope"></i>
                                <a href="mailto:{{ Config::get('constants.CONTACT_EMAIL_3') }}" title="Email {{ Config::get('constants.SITE_NAME') }}">
                                    {{ Config::get('constants.CONTACT_EMAIL_3') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="copyright">
             {{ Config::get('constants.SITE_NAME') }}  © {{ Carbon\Carbon::today()->format('Y') }}. All Rights Reserved | Website Developed by <a href="https://lpwebdesign.co.uk" title="LP Web Design" target="_blank">LP Web Design</a>
        </div>
