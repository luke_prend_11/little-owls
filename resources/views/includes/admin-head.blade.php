    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ Meta::$pageTitle }}</title>
    <meta name="Developer" content="Luke Prendergast" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet/less" type="text/css" href="{{ URL::asset('css/admin.less') }}" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Muli:300,400,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('img/favicon.png') }}" />
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/less.min.js') }}"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>