            <div id="information-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="pull-left">
                                <ul>
                                    <li class="show-500"><strong>{{ Config::get('constants.SITE_NAME') }}</strong></li>
                                    <i class="show-500"></i>
                                    <li class="show-900">
                                        <a href="{{ route('contact') }}#avenue-vivian" title="{{ Config::get('constants.CONTACT_ADDRESS_ROAD_1') }}">
                                            {{ Config::get('constants.CONTACT_ADDRESS_ROAD_1') }}
                                        </a>
                                        <a href="tel:{{ Config::get('constants.CONTACT_PHONE_1') }}" title="Call {{ Config::get('constants.SITE_NAME') }}">
                                            <i class="fas fa-phone"></i>{{ Config::get('constants.CONTACT_PHONE_1') }}
                                        </a>
                                    </li>
                                    <i class="show-900"></i>
                                    <li class="show-900">
                                        <a href="{{ route('contact') }}#dragonby-road" title="{{ Config::get('constants.CONTACT_ADDRESS_ROAD_2') }}">
                                            {{ Config::get('constants.CONTACT_ADDRESS_ROAD_2') }}
                                        </a>
                                        <a href="tel:{{ Config::get('constants.CONTACT_PHONE_2') }}" title="Call {{ Config::get('constants.SITE_NAME') }}">
                                            <i class="fas fa-phone"></i>{{ Config::get('constants.CONTACT_PHONE_2') }}
                                        </a>
                                    </li>
                                    <i class="show-900"></i>
                                    <li class="show-900">
                                        <a href="{{ route('contact') }}#cowper-avenue" title="{{ Config::get('constants.CONTACT_ADDRESS_ROAD_3') }}">
                                            {{ Config::get('constants.CONTACT_ADDRESS_ROAD_3') }}
                                        </a>
                                        <a href="tel:{{ Config::get('constants.CONTACT_PHONE_3') }}" title="Call {{ Config::get('constants.SITE_NAME') }}">
                                            <i class="fas fa-phone"></i>{{ Config::get('constants.CONTACT_PHONE_3') }}
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="pull-right">
                                <ul>
                                    <li>
                                        <a href="mailto:{{ Config::get('constants.CONTACT_EMAIL') }}" title="Email {{ Config::get('constants.SITE_NAME') }}">
                                            <i class="fas fa-envelope"></i>
                                        </a>
                                    </li>
                                    <li>
                                    <i></i>
                                        <a href="https://facebook.com/{{ Config::get('constants.FACEBOOK_ID') }}" title="{{ Config::get('constants.SITE_NAME') }} on Facebook" target="_blank">
                                            <i class="fab fa-facebook-square"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="header-image">
                <div class="container">
                    <a id="logo" href="{{ route('home') }}" title="{{ Config::get('constants.SITE_NAME') }}"></a>
                </div>
            </div>
    
            <nav id="navbar" class="navbar">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-brand" id="logo" href="{{ route('home') }}" title="Home Page">
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav list-inline">
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('eyfs') }}" title="Early Years Foundation Stage">Early Years Foundation Stage</a></li>
                                    <li><a href="{{ route('parental-essentials') }}" title="Information for Parents">Parental Essentials</a></li>
                                    <li><a href="{{ route('term-dates') }}" title="{{ Config::get('constants.SITE_NAME') }} Term Dates">School Term Dates</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Our Staff <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('staff') }}" title="Meet Our Staff">Staff Members</a></li>
                                    <li><a href="{{ route('staff-training') }}" title="Staff Training">Training</a></li>
                                    <li><a href="{{ route('staff-keyperson') }}" title="Key Person Policy">Key Person Policy</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ route('newsletters') }}" title="Latest Newsletters">Newsletters</a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Curriculum <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('curriculum-communication') }}" title="Communication &amp; Language Development">Communication &amp; Language Development</a></li>
                                    <li><a href="{{ route('curriculum-ea') }}" title="Expressive Arts &amp; Design">Expressive Arts &amp; Design</a></li>
                                    <li><a href="{{ route('curriculum-literacy') }}" title="Literacy">Literacy</a></li>
                                    <li><a href="{{ route('curriculum-maths') }}" title="Mathematics">Mathematics</a></li>
                                    <li><a href="{{ route('curriculum-personal') }}" title="Personal Social &amp; Emotional Development">Personal Social &amp; Emotional Development</a></li>
                                    <li><a href="{{ route('curriculum-pd') }}" title="Physical Development">Physical Development</a></li>
                                    <li><a href="{{ route('curriculum-world') }}" title="Understanding The World">Understanding The World</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Policies <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('policies-admissions') }}" title="Admissions Policy">Admissions</a></li>
                                    <li><a href="{{ route('policies-behaviour') }}" title="Behaviour Management Policy">Behaviour Management</a></li>
                                    <li><a href="{{ route('policies-child-protection') }}" title="Child Protection Policy">Child Protection</a></li>
                                    <li><a href="{{ route('policies-complaints') }}" title="Complaints Policy">Complaints</a></li>
                                    <li><a href="{{ route('policies-confidentiality') }}" title="Confidentiality Policy">Confidentiality</a></li>
                                    <li><a href="{{ route('policies-equal-opportunities') }}" title="Equality of Opportunities Policy">Equality of Opportunities</a></li>
                                    <li><a href="{{ route('policies-fees-and-funding') }}" title="Fees &amp; Funding Policy">Fees &amp; Funding</a></li>
                                    <li><a href="{{ route('policies-healthy-eating') }}" title="Healthy Eating Policy">Healthy Eating</a></li>
                                    <li><a href="{{ route('policies-intimate-care') }}" title="Intimate Care Policy">Intimate Care</a></li>
                                    <li><a href="{{ route('policies-late-collection') }}" title="Late Collection Policy">Late Collection</a></li>
                                    <li><a href="{{ route('policies-medication') }}" title="Medication Policy">Medication</a></li>
                                    <li><a href="{{ route('policies-mobile-phone') }}" title="Mobile Phone &amp; Social Networking Policy">Mobile Phone &amp; Social Networking</a></li>
                                    <li><a href="{{ route('policies-smoking') }}" title="No Smoking Policy">No Smoking</a></li>
                                    <li><a href="{{ route('policies-settling-in') }}" title="Settling in Policy">Settling In</a></li>
                                    <li><a href="{{ route('policies-transition') }}" title="Transition Policy">Transition</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ route('contact') }}" title="Contact {{ Config::get('constants.SITE_NAME') }}">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            