
<div class="news-item">
    <h4>{{ $holidays[0]->Holiday->name }}</h4>
    @foreach($holidays as $holiday)
    @if(count($holidays) > 1)
    <h5>{{ $holiday->school->location }}</h5>
    @endif
    <p><strong>Close</strong>: {{ Carbon\Carbon::parse($holiday->closes)->format('l j F Y') }}</p>
    <p><strong>Re-open</strong>: {{ Carbon\Carbon::parse($holiday->re_opens)->format('l j F Y') }}</p>
    @endforeach
</div>