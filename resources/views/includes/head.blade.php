    @if (App::environment('production'))
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-92671796-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-92671796-2');
    </script>
    @endif 

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ Meta::$pageTitle }}</title>
    <meta name="Description" content="{{ Meta::$metaDescription }}" />
    <meta name="Keywords" content="{{ Meta::$metaKeywords }}" />
    <meta name="Developer" content="Luke Prendergast" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet/less" type="text/css" href="{{ URL::asset('css/master.less') }}" />
    {!! Meta::$css !!}
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Muli:300,400,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('img/favicon.png') }}" />

    <!-- Force html5 elements to work in ie7 and ie8 -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/less.min.js') }}"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>