<h2 id="{{ str_replace("/", "", $schoolYear->name) }}">{{ $schoolYear->name }}</h2>

<div class="col-xs-12 col-md-12 no-padding year">
    <div class="title">
        <span class="col-xs-4 no-padding">&nbsp;</span>
        <span class="col-xs-4 no-padding">Closes</span>
        <span class="col-xs-4 no-padding">Re-opens</span>
    </div>
    @foreach($schoolYearHolidays as $schoolYearHoliday)
        @if(count($schoolYearHoliday->school_holiday) > 0)
        <div class="holidays">
            @foreach($schoolYearHoliday->school_holiday as $key => $schoolHoliday)
                <div class="child">
                    @if($loop->first)
                    <span class="col-xs-4 no-padding name">
                        <h3>{{ $schoolYearHoliday->name }}</h3>
                    </span>
                    <span class="col-xs-4">
                    @else
                    <span class="col-xs-4 col-xs-offset-4">
                    @endif
                        {{ Carbon\Carbon::parse($schoolHoliday->closes)->format('l j F Y') }}
                    </span>
                    <span class="col-xs-4">
                        {{ Carbon\Carbon::parse($schoolHoliday->re_opens)->format('l j F Y') }}
                        @if(($loop->first && $loop->last) || (isset($schoolYearHoliday->school_holiday[$key+1]) && $schoolYearHoliday->school_holiday[$key+1]->closes != $schoolHoliday->closes))
                            <strong>(All)</strong>
                        @else
                            <strong>({{ $schoolHoliday->school->location }})</strong>
                        @endif
                    </span>
                </div>
            @endforeach
        </div>
        @endif
    @endforeach

    @if(count($schoolYearBankHolidays) > 0)
    <h3>Bank Holidays</h3>
    @foreach($schoolYearBankHolidays as $schoolYearBankHoliday)
        <div class="holidays">
            <span class="col-xs-4 no-padding name">
                <h3>{{ $schoolYearBankHoliday->holiday->name }}</h3>
            </span>
            <span class="col-xs-8 no-padding">{{ Carbon\Carbon::parse($schoolYearBankHoliday->closes)->format('l j F Y') }}</span>
        </div>
    @endforeach
    @endif
</div>
