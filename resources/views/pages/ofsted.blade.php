@extends('layouts.master')
@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-9">
        <p>Our most recent Ofsted inspection was in October 2013 and gave us an overall judgement of <strong>Outstanding</strong>!. You can download this report or older reports by clicking on the links below. Or you can visit the <a href="https://www.gov.uk/government/organisations/ofsted" title="Ofsted Website" target="_blank">Ofsted website</a> for more information.</p>

        <h2>Ofsted Reports</h2>
        <h3>October 2013</h3>
        <a href="{{ URL::asset("downloads/Ofsted_2013.pdf") }}" class="btn btn-primary btn-block" title="{{ config('constants.SHORT_SITE_NAME') }} Ofsted Report October 2013">
            Read Report
        </a>

        <h3>October 2011</h3>
        <a href="{{ URL::asset("downloads/Ofsted_2011.pdf") }}" class="btn btn-primary btn-block" title="{{ config('constants.SHORT_SITE_NAME') }} Ofsted Report October 2011">
            Read Report
        </a>
    </div>
    
    <div class="col-xs-12 col-sm-4 col-md-3">
        <div id="ofsted-logo"></div>
    </div>
</div>

@stop
