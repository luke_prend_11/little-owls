@extends('layouts.master')
@section('content')

@if(isset($success))
<div class="alert alert-success">
    <strong>Success!</strong> {{ $success }}
</div>
@endif

<p>If you would like any further information regarding <strong>{{ config('constants.SITE_NAME') }}</strong> or if you would like to apply for a place then contact us using the details below. Or alternatively, fill in the form and we will get back to you as soon as possible.</p>

<div class="row">
    <div class="col-md-6">
        <h3>Contact Details</h3>
        <ul class="list-group">
            <li>
                <i class="fas fa-phone"></i>
                <a href="tel:{{ Config::get('constants.CONTACT_PHONE_1') }}" title="Call {{ Config::get('constants.SITE_NAME') }}">
                    {{ Config::get('constants.CONTACT_PHONE_1') }}
                </a>
            </li>
            <li>
                <i class="fas fa-envelope"></i>
                <a href="mailto:{{ Config::get('constants.CONTACT_EMAIL') }}" title="Email {{ Config::get('constants.SITE_NAME') }}">
                    {{ Config::get('constants.CONTACT_EMAIL') }}
                </a>
            </li>
            <li>
                <i class="fab fa-facebook-square"></i>
                <a href="https://facebook.com/{{ Config::get('constants.FACEBOOK_ID') }}" title="{{ Config::get('constants.SITE_NAME') }} on Facebook">
                    {{ Config::get('constants.FACEBOOK_LINK') }}
                </a>
            </li>
        </ul>
    </div>

    <div class="col-md-6">
        <h3>Contact Form</h3>
        <form class="form-horizontal" method="POST" action="{{ route('contact-form-submit') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <input id="name" type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control" placeholder="E-Mail Address" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">
                <textarea id="comments" class="form-control" placeholder="Comments" name="comments" required></textarea>

                @if ($errors->has('comments'))
                    <span class="help-block">
                        <strong>{{ $errors->first('comments') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
            </div>
        </form>
    </div>
</div>

<h2 id="our-locations">Our Locations</h2>
<div id="locations">
    <div id="avenue-vivian" class="row locations">
        <div class="col-xs-12 col-md-2 address">
            <h3>{{ Config::get('constants.CONTACT_ADDRESS_ROAD_1') }}</h3>
            <p>
                {{ Config::get('constants.CONTACT_ADDRESS_BUILDING_1') }}<br />
                {{ Config::get('constants.CONTACT_ADDRESS_ROAD_1') }}<br />
                {{ Config::get('constants.CONTACT_ADDRESS_TOWN_1') }}<br />
                {{ Config::get('constants.CONTACT_ADDRESS_COUNTY_1') }}<br />
                {{ Config::get('constants.CONTACT_ADDRESS_POSTCODE_1') }}
            </p>
        </div>
        <div class="col-xs-12 col-md-10 map">
            <iframe
                frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCzWW-AtYWjVF5kG6Ow3hGR_fVkW3hB9ps
                  &q=Little+Owls+Nursery+Avenue+Vivian" allowfullscreen>
            </iframe>
        </div>
    </div>

    <div id="dragonby-road" class="row locations">
        <div class="col-xs-12 col-md-2 address">
            <h3>{{ Config::get('constants.CONTACT_ADDRESS_ROAD_2') }}</h3>
            <p>
                {{ Config::get('constants.CONTACT_ADDRESS_ROAD_2') }}<br />
                {{ Config::get('constants.CONTACT_ADDRESS_TOWN_2') }}<br />
                {{ Config::get('constants.CONTACT_ADDRESS_COUNTY_2') }}<br />
                {{ Config::get('constants.CONTACT_ADDRESS_POSTCODE_2') }}
            </p>
        </div>
        <div class="col-xs-12 col-md-10 map">
            <iframe
                frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCzWW-AtYWjVF5kG6Ow3hGR_fVkW3hB9ps
                  &q=Little+Owls+Nursery+Dragonby+Road" allowfullscreen>
            </iframe>
        </div>
    </div>

    <div id="cowper-avenue" class="row locations">
        <div class="col-xs-12 col-md-2 address">
            <h3>{{ Config::get('constants.CONTACT_ADDRESS_ROAD_3') }}</h3>
            <p>
                {{ Config::get('constants.CONTACT_ADDRESS_ROAD_3') }}<br />
                {{ Config::get('constants.CONTACT_ADDRESS_TOWN_3') }}<br />
                {{ Config::get('constants.CONTACT_ADDRESS_COUNTY_3') }}<br />
                {{ Config::get('constants.CONTACT_ADDRESS_POSTCODE_3') }}
            </p>
        </div>
        <div class="col-xs-12 col-md-10 map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2369.2615317173213!2d-0.6800110841530729!3d53.57094888002676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4878e4cf0e6de28f%3A0xf1099b453f6ee177!2sLichfield+Ave%2C+Scunthorpe+DN17+1QL!5e0!3m2!1sen!2suk!4v1560352120835!5m2!1sen!2suk" width="600" height="450" frameborder="0" style="border:0" allowfullscreen>

            </iframe>
        </div>
    </div>
</div>

@stop
