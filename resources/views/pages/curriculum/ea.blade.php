@extends('layouts.master')
@section('content')

<p>Expressive arts and design is fundamental to successful learning. Being creative enables children to make connections between one area of learning and another and so extend their understanding.</p>
<p>This area of learning includes art, music, dance, role play, and imaginative play.</p>
<p><strong>We aim to do this by:</strong></p>

<ul>
    <li>Modeling, encouraging and praising children’s creative work.</li>
    <li>Provide a wide range of creative opportunities, i.e. glue, paint, gloop,  chalk crayons, string painting, candle painting, finger painting, collage  materials, junk modeling etc.</li>
    <li>Allowing and encouraging children to be independent, to explore, make  choices and use self- expression.</li>
    <li>Ensure that the children have time to finish their work.</li>
    <li>Provide plenty of opportunities for children to explore music, musical  instruments and movement.</li>
    <li>Provide other creative opportunities such as cooking, gardening, role  play.</li>
    <li>Ensure all children have opportunities to explore</li>
</ul>

@stop
