@extends('layouts.master')
@section('content')

<p>Children are learning communication skills every day in every situation. They learn new words, and the difficult skill of taking turns in conversation by watching and listening to the people around them.</p>
<p>Before a child can begin to read and write, they need many other skills in place first. They need to develop good hand eye co-ordination.</p>
<p><strong>We aim to do this by:</strong></p>
<ul>
    <li>Planning fine-motor, free play activities every day, to encourage good hand eye co-ordination e.g. threading, jigsaws, hammer and nails, tweezers, play dough, computer and printer, construction, sand, water etc.</li>
    <li>Providing many different opportunities for mark making, not just pencils and crayons, e.g. sand trays, scissors, gloop, ink stamps, chalks, painting  with sponges, rollers, string, candles, printing with stencils, vegetables,  fruit, hands and feet.</li>
    <li>Providing different sizes, colours, shapes of paper, card, white boards, magazines etc. for the children to mark make on.</li>
    <li>Creating a language rich environment using pictures, leaflets, signs, labels, hand written and printed using different fonts, styles and sizes.</li>
    <li>Ensuring children's mark making pieces of work are displayed around the Pre-School.</li>
    <li>Changing role play area, doctors, hairdressers, travel agents, food  shops etc.</li>
    <li>Well stocked book corner, picture books, large print books, fiction,  non- fiction, rhyming books etc.</li>
    <li>Group story time, individual story time.</li>
    <li>Sharing rhymes, books, poems, songs, using props i.e. puppets, story sacks.</li>
    <li>Group circle times, modeling turn taking.</li>
    <li>Being fully inclusive to all children.</li>
    <li>Showing an awareness and sensitivity to each child's level of communication skills.</li>
    <li>Encouraging children to bring things in from home to share as a group.</li>
    <li>Using video recorders and microphones.</li>
    <li>Ensuring all children have access to the listening corner, either individually or in small groups</li>
    <li>Planning activities which give the children an opportunity to learn new vocabulary, talking about what they  are seeing and doing, what's changing e.g. bread making, cooking, making sandwiches, paper mache, junk modeling etc.</li>
    <li>Liaising with parents/carers of children who have English as a second language. Encouraging the use of their own language at home and within the Pre-School.</li>
    <li>Identifying if a child may need extra support and ensuring with parents/carers permission that advice is sought if the staff feel a child needs help and support with their communication skills.</li>
    <li>GIVING THE CHILDREN TIME TO TALK WITH THEIR PEERS AND STAFF.</li>
</ul>

@stop
