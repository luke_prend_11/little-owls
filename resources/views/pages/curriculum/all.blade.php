@extends('layouts.master')
@section('content')

<p>Click on a curriculum below to read more information.</p>
<ul class="list-unstyled">
    <li><a href="{{ route('curriculum-personal') }}" title="Personal Social &amp; Emotional Development">Personal Social &amp; Emotional Development</a></li>
    <li><a href="{{ route('curriculum-communication') }}" title="Communication &amp; Language Development">Communication &amp; Language Development</a></li>
    <li><a href="{{ route('curriculum-pd') }}" title="Physical Development">Physical Development</a></li>
    <li><a href="{{ route('curriculum-literacy') }}" title="Literacy">Literacy</a></li>
    <li><a href="{{ route('curriculum-maths') }}" title="Mathematics">Mathematics</a></li>
    <li><a href="{{ route('curriculum-world') }}" title="Understanding The World">Understanding The World</a></li>
    <li><a href="{{ route('curriculum-ea') }}" title="Expressive Arts &amp; Design">Expressive Arts &amp; Design</a></li>
</ul>

@stop
