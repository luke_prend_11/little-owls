@extends('layouts.master')
@section('content')

<p>Mathematical development depends on the children becoming confident and competent in learning and using key skills.</p>
<p>This area of learning includes counting, sorting, matching, seeing patterns, making connections, working with numbers, shapes, space and measures.</p>
<p><strong>We aim to do this by:</strong></p>

<ul>
    <li>Providing plenty of opportunities for story time and story telling.</li>
    <li>Singing and acting out number rhymes.</li>
    <li>Using props as visual clues for the rhymes.</li>
    <li>Role play areas with pencils, calculators, tills.</li>
    <li>Different jigsaws and puzzles to promote shape and space awareness.</li>
    <li>Physical activities inside and outside.</li>
    <li>Table activities such as lotto, dominoes, hammer and nails etc.</li>
    <li>Encourage the children to predict and estimate what will happen.</li>
    <li>Number lines on the wall, using velcro, so enabling the children to  re-arrange and order number lines.</li>
    <li>Using everyday activities to encourage children to think about numbers, i.e. Snack time how many children are sat at the table, how many are at the computer table, how many plates do we need in the home corner?</li>
    <li>Date and time line so the children become familiar with the vocabulary.</li>
    <li>Using mathematical language in everyday situations, big, small, little, large, corners, sides, greater than, more than, less than, etc.</li>
    <li>Experimenting with shapes and measures using sand, water, gloop, playdough.</li>
    <li>Junk modelling.</li>
    <li>Cooking, encouraging the children to weigh and measure the ingredients.</li>
</ul>

@stop
