@extends('layouts.master')
@section('content')

<p>Successful PSED is critical for all young children in all aspects of their lives, in order to give them the best opportunity for success in the other areas of learning.</p>
<p>We want all our children to be valued members of the Pre-School and the wider community and to develop a strong self-image and  positive self-esteem.</p>
<p><strong>We aim to do this by:</strong></p>

<ul>
    <li>Supporting the child and their family through the transition into Pre-school using key workers and settling in forms.</li>
    <li>Promoting good home / Pre-School partnership.</li>
    <li>Promoting an inclusive ethos.</li>
    <li>Treating each child as unique, remembering that each child's needs are  different.</li>
    <li>Recognising the importance of praise.</li>
    <li>Promoting and encouraging child initiated play.</li>
    <li>Ensuring a welcoming, safe, warm and caring atmosphere within Pre-School.</li>
    <li>Recognising the importance of children sharing news, books, pictures etc  brought in from home, with the Pre-School.</li>
    <li>Promoting children's independent choosing through different activities,  and self- service snack bar.</li>
    <li>Encouraging children's independence and self- help skills with the snack  bar, self- registration, going to the toilet, washing hands and tidying up etc.</li>
    <li>Providing a well-stocked role play area with changing themes</li>
    <li>Planning circle times to allow each child an opportunity to chat to the  group if they want.</li>
    <li>Displaying photographs of the children and pieces of their work around  the Pre-School.</li>
    <li>Involving all our children (if they wish to take part) in our Christmas  Nativity.</li>
    <li>Celebrating various festivals and customs throughout the year.</li>
    <li>Using puppets and games to promote emotional, spiritual, moral and social development.</li>
    <li>Ensuring all staff are good role models.</li>
</ul>

@stop
