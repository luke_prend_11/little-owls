@extends('layouts.master')
@section('content')

<p>Physical development is about improving skills of coordination, control, manipulation and movement.</p>
<p>Physical development helps children to gain  confidence in what they can do and enables them to feel the positive benefits of being healthy and active. Effective physical development helps children develop a positive sense of well-being.</p>
<p><strong>We aim to do this by:</strong></p>
<ul>
    <li>Providing a range of equipment, inside and outside to encourage gross  motor skills, i.e. Bikes, trikes, scooters, slide, balls, bean bags, hoops,  assault course, etc.</li>
    <li>Using musical instruments and music to provide movement and exercise.</li>
    <li>Providing a range of equipment to encourage fine motor skills, i.e.  threading, hammer and nails, chalks, crayons, playdough, gloop, construction  activities, scissors and creative play.</li>
    <li>Encourage physical independence, children to hang their own coats,  aprons up, to put their own coats, shoes, aprons, role play outfits on.</li>
    <li>Children are encouraged to use the toilet and wash hands independently.</li>
    <li>Provide sufficient space to enable the children to move from activity to  activity inside and outside, safely and with confidence.</li>
    <li>Ensure children’s safety whilst allowing appropriate physical  challenges.</li>
    <li>Using appropriate language and vocabulary alongside physical activities.</li>
    <li>Giving the children, time, encouragement and praise.</li>
    <li>Providing plenty of opportunity for action songs, games and rhymes.</li>
    <li>Ensuring a healthy snack bar, providing the children with choice.</li>
    <li>Providing fresh drinking water every day, which the children can access  independently.</li>
    <li>Talking about and encouraging healthy eating, providing parents with  leaflets and advice for a healthy lunch box.</li>
    <li>Using opportunities such as circle time or after physical activities to  discuss how we feel, why we get tired, why we feel hot.</li>
</ul>

@stop
