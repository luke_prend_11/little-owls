@extends('layouts.master')
@section('content')

<h2 id="positive-relationships">Positive Relationships</h2>
<p>Daily opportunities given to enjoy a wide range of fiction, non-fiction books, rhymes, music and songs. Encourage and support children's responses to picture books and stories that you read with them and  encourage children to use the stories they hear in their play. Provide dual language books and read them with all children, to raise awareness of different scripts. Try to match dual language books to languages spoken by families in the setting.</p>
<h2 id="enabling-environment">Enabling Environment</h2>
<p>Plan an environment that is rich in signs, symbols, numbers, words, rhymes, books, pictures, music, song and take into account children's different interest, understanding, home background and culture. Show particular awareness of, and sensitivity to, the needs of  children learning English as an additional language.</p>
<p>Provide CD's of rhymes stories, sounds and spoken words and find opportunities to tell and read stories to children, using puppets, soft toys, or real objects as props.</p>

@stop
