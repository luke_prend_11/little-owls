@extends('layouts.master')
@section('content')

<p>In this area children are developing the crucial knowledge, skills and understanding that help them to make sense of the world, and who they are.</p>
<p>This forms the foundation for later work in science, design and technology, history, geography and information and communication technology.</p>
<p><strong>We aim to do this by:</strong></p>
<ul>
    <li>Providing opportunities for the children to explore, observe, investigate and think about outcomes.</li>
    <li>Use activities to promote the above such as, gloop, cookery, bread making, painting and modeling with different media, junk modeling, sand, water, growing cress, plants, weather charts, time lines.</li>
    <li>Enjoy and explore experiments such as freezing, melting, making jelly  etc.</li>
    <li>Record outcomes and results in different ways, using pictures, graphs,  mark making, etc.</li>
    <li>Encouraging use of construction activities, stickle bricks, duplo, lego,</li>
    <li>magnetic shapes, cogs and wheels, hammer and nails.</li>
    <li>Provide a large range of cd’s, tapes, books, rhymes for the children to use in the listening corner.</li>
    <li>To encourage independence when using ICT toys.</li>
    <li>To support the children who are not confident using the ICT equipment.</li>
    <li>To talk about our home life and families.</li>
    <li>Encourage the children to bring in pictures, favourite things from home.</li>
    <li>Provide opportunities for the children to talk about their families, either as a group or on a one to one.</li>
    <li>Plan trips to local areas,  local  primary schools, walk down to the local shops.</li>
    <li>Welcome and encourage visitors to the Pre-School, i.e. lollipop lady, fire service, invite visitors relevant to the topic.</li>
    <li>Celebrate and talk about different religious celebrations, Divali, Hannukah, Chinese New year, Christmas etc.</li>
    <li>Invite parents / carers / grandparents to take part in different celebrations.</li>
    <li>Encourage the children to think about time, present, past and future, using picture lines, time lines, photographs, clocks.</li>
    <li>Changing role play areas, different themes linking to the children’s interest.</li>
    <li>Plenty of circle time opportunities to talk about news from home, topic  discussions, anything of interest or just time for a chat.</li>
    <li>Listen and appreciate what the children have to say, allowing them time  to answer and express their feelings and thoughts.</li>
</ul>

@stop
