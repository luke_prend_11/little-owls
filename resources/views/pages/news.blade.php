@extends('layouts.master')
@section('content')

@if(count($latestNews) > 0)
    <div id="latest" class="{{ count($olderNews) > 0 ? 'col-xs-12 col-md-7' : 'col-xs-12' }} no-padding">
        <h2>Latest News</h2>
        
        @foreach($latestNews as $news)
        <div>
            <h3>{{ $news->title }}</h3>
            <span class="date">{{ date('j F Y', strtotime($news->updated_at)) }}</span>
            <p>{!! $news->contents !!} {{ $news->link_text }}.</p>

            @if($news->button_text)
                <a class="btn btn-primary btn-block" href="{{ URL::asset('downloads/'.$news->download->file_name) }}">{{ $news->button_text }}</a>
            @endif
        </div>
        @endforeach
    </div>
@endif
      
@if(count($olderNews) > 0)
    <div id="old" class="col-xs-12 col-md-5 no-padding">
        <h2>Older News</h2>
        
        @foreach($olderNews as $news)
        <div>
            <h3>{{ $news->title }}</h3>
            <span class="date">{{ date('j F Y', strtotime($news->updated_at)) }}</span>
            <p>{!! $news->contents !!} {{ $news->link_text }}.</p>

            @if($news->button_text)
                <a class="btn btn-secondary btn-block" href="{{ URL::asset('downloads/'.$news->download->file_name) }}">{{ $news->button_text }}</a>
            @endif
        </div>
        @endforeach
    </div>
@endif
        
@stop
