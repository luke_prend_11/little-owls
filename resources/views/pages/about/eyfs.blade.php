@extends('layouts.master')
@section('content')

<p>Like all <a href="http://www.ofsted.gov.uk/" title="Ofsted" target="_blank">Ofsted</a>-registered childcare settings, Little Owls must follow a structure of learning, development and care for children from birth to 5 years. This is called the Early Years Foundation Stage (EYFS) and it enables your child to learn through a range of activities and prepare them for success with the primary school curriculum.</p>
<p>The EYFS learning and development requirements comprise:</p>
<ul>
    <li>The seven areas of learning and development and the educational programmes.</li>
    <li>The early learning goals, which summarise the knowledge, skills and understanding that all young children should have gained by the end of the Reception year.</li>
    <li>The assessment requirements.</li>
</ul>

<h2>Areas of Learning and Development</h2>
<p>There are seven areas of learning and development that must shape educational programmes in early years settings. All areas of learning and development are important and inter-connected. Three areas are particularly crucial for igniting children’s curiosity and enthusiasm for learning, and for building their capacity to learn, form relationships and thrive. These three areas, the prime areas, are:</p>
<ul>
    <li><a href="{{ route('curriculum-communication') }}" title="Communication, Language, Literacy">Communication and language</a></li>
    <li><a href="{{ route('curriculum-pd') }}" title="Physical Development">Physical development</a></li>
    <li><a href="{{ route('curriculum-personal') }}" title="Personal, Social, Emotional">Personal, social and emotional development</a></li>
</ul>
<p>Little Owls also support children in four specific areas, through which the three prime areas are strengthened and applied. The specific areas are:</p>
<ul>
    <li><a href="{{ route('curriculum-literacy') }}" title="Literacy">Literacy</a></li>
    <li><a href="{{ route('curriculum-maths') }}" title="Problem Solving, Reasoning, Numeracy">Mathematics</a></li>
    <li><a href="{{ route('curriculum-world') }}" title="The World">Understanding the World</a></li>
    <li><a href="{{ route('curriculum-ea') }}" title="Creative Development">Expressive arts and design</a></li>
</ul>
<p>Little Owls will consider the individual needs, interests, and stage of development of each child in their care, and will use this information to plan a challenging and enjoyable experience for each child in all of the areas of learning and development.</p>
<p>When working with the youngest children we will focus strongly on the three prime areas, which are the basis for successful learning in the other four specific areas.</p>
<p>The three prime areas reflect the key skills and capacities all children need to develop and learn effectively, and become ready for school. It is expected that the balance will shift towards a more equal focus on all areas of learning as children grow in confidence and ability within the three prime areas.</p>
<p>Throughout the early years, if a child’s progress in any prime area gives cause for concern, we will discuss this with the child’s parents and/or carers and agree how to support the child. We will consider whether a child may have a special educational need or disability which requires specialist support.</p>
<p>We will link with, and help families to access relevant services from other agencies as appropriate.</p>
<p>For children whose home language is not English, providers must take reasonable steps to provide opportunities for children to develop and use their home language in play and learning, supporting their language development at home.</p>
<p>In order for the children at Little Owls to have effective teaching and learning each child will have their own Key Person and this person will help to ensure that  every child’s learning and care is tailored to meet their individual needs.</p>
<p>The Key Person will plan and guide children’s activities and reflect on different ways that children learn.</p>
<p>Three characteristics of effective teaching and learning are:</p>
<ul>
    <li>Playing and exploring - children investigate and experience things, and ‘have a go’.</li>
    <li>Active learning - children concentrate and keep on trying if they encounter difficulties, and enjoy.</li>
    <li>Creating and thinking critically - children have and develop their own ideas, make links between ideas, and develop strategies for doing things.</li>
</ul>
<p>For further information about the Early Years Foundation Stage, visit the <a href="http://www.education.gov.uk/childrenandyoungpeople/earlylearningandchildcare/delivery/education/a0068102/early-years-foundation-stage-eyfs" target="_blank">Department of Education</a>.</p>

@stop
