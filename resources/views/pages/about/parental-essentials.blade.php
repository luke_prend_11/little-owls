@extends('layouts.master')
@section('content')

<h2 id="applying-for-a-place">Applying for a Place</h2>
<p>Little Owls is extremely popular but we only have a limited number of child places. If you would like your child to attend we advise you apply for a place as soon as possible, <a href="{{ route('contact') }}" title="Contact Little Owls">click here</a> to apply now or call Sharon or Kaye on 01724 281050.</p>

<h2 id="sessions">Sessions</h2>
<ul>
    <li>Monday to Friday - 8.00am to 4.00pm at <strong>{{ Config::get('constants.CONTACT_ADDRESS_ROAD_1') }}</strong></li>
    <li>Monday to Friday - 9.00pm to 4.00pm at <strong>{{ Config::get('constants.CONTACT_ADDRESS_ROAD_2') }}</strong></li>
    <li>Monday to Friday - 9.00am to 4.00pm at <strong>{{ Config::get('constants.CONTACT_ADDRESS_ROAD_3') }}</strong></li>
    <li>Hours must be contracted prior to commencement.</li>
</ul>
<p><strong><a href="{{ route('term-dates') }}" title="Term Dates">Click here</a> for term start and end dates.</strong></p>

<h2 id="extra-sessions">Extra Sessions</h2>
<p>Before and after school sessions are available on request. Call Sharon or Kaye on 01724 281050.</p>

<h2 id="fees-and-payment">Fees and Payment</h2>
<p>Session fees are £5.50 per hour. An invoice will be created on the last Friday of the month and must be paid for by the 15th of the month.</p>
<p>Please make payment by cash, cheque or bank transfer.(Details can be obtained from the management) Cheques payable to Little Owls (Scunthorpe) Ltd.</p>
<p>You will still be invoiced for sessions missed due to illness, holiday or other reasons.</p>
<p>If you are having trouble making payment please speak to Sharon or Kaye on 01724 281050.</p>

<h2 id="getting-help-with-childcare-costs">Getting Help with Childcare Costs</h2>
<p>Your child may be entitled to 2 year funding for up to 15 hours if you fit within the eligible criteria for more information ring Family Information Services (01724) 296629.</p>
<p>There are a few ways you can receive financial help with childcare costs:</p>
<ul>
    <li>All three and four year olds are entitled to 15 hours of free nursery education for 38 weeks a year. Please discuss this with Sharon or Kaye if you feel your child is eligible.</li>
    <li>If you are a working parent you may be entitled to extra tax credits to help with childcare costs.</li>
    <li>Your employer can use a childcare voucher scheme so you don't pay tax and national insurance for the part of your salary used to pay for childcare costs. Little Owls accept various childcare vouchers, and can register for other schemes if required. <a href="http://www.gov.uk/free-early-education" title="Financial Support for Parents" target="_blank">Click here</a> for further details about getting help with childcare costs.</li>
    <li>30 hours childcare.</li>
    <li>You may be able to get up to 30 hours free childcare (1,140 hours per year, which you can choose how you take).</li>
    <li>If you’re eligible for the extra hours, you sign up online to get a code to give to your childcare provider to reserve your place. You’ll get the extra hours once the next term starts. <a href="https://www.childcarechoices.gov.uk" title="Childcare Choices" target="_blank">Apply here for 30 hours free childcare</a>.</li>
</ul>

<h2 id="late-collection">Late Collection</h2>
<p>The standard hourly fee will be imposed for the late collection of your child. In addition, a £15 fine will be imposed for late collection.</p>

<h2 id="illness-and-exclusion">Illness and Exclusion</h2>
<p>Please telephone the setting your child attends to inform them of any illness or absence as soon as possible otherwise your child will be marked with an unauthorised absence.</p>

<h2 id="clothing-and-footwear">Clothing and Footwear</h2>
<p>We advise you put name tags on the clothes and footwear your child wears to pre-school.</p>
<p>Please do not &quot;dress your child up&quot; to come to pre-school. Despite our best efforts, children do have a habit of getting paint and glue on their clothing!</p>
<p>A change of clothing should remain in your child's bag.</p>
<p>If your child arrives in wellington boots please bring some appropriate footwear for them to change into before going into the setting.</p>

<h2 id="personal-items-and-toys">Personal Items and Toys</h2>
<p>Please do not bring in personal items and toys because it is likely they will become lost amongst our own extensive collection!</p>
<p>If your child has a comfort item or toy they insist on bringing to pre-school, it can  be left in their school bag.</p>

<h2 id="eating--drinking">Eating &amp; Drinking</h2>
<p>During the morning session your child will be offered a drink of milk or water and a healthy snack such as fruit, sandwich, cheese and crackers, breadsticks, etc.</p>
<p>Lunch is not provided and you are expected to supply your child with healthy food and drink for full day. <a href="{{ URL::asset('downloads/healthy-packed-lunches.pdf') }}" title="Healthy Food Poster">Click here</a> to view some healthy packed lunch ideas.</p>
<p>Please  clearly label your child's lunch box and drink container with their name. On arrival these should be placed on the dinner trolley just in the pre-school building.</p>
<p>Please advise us in writing about food allergies and strong dislikes your child may have.</p>

<h2 id="sun-care">Sun Care</h2>
<p>Please  provide a sun hat and sun cream with your child’s name on during every session your child attends. On sunny days we  are unable to allow your child into the outside area without them - even during the Autumn when the sun can still be strong.</p>
<p>Sun hats and sun cream should remain in your child's school bag.</p>

<h2 id="mapping-and-sharing-booklet">Mapping and Sharing Booklet</h2>
<p>Throughout your child's attendance at pre-school their key person completes a Progress Book. This is a record of your child's progress in the Early Years Foundation Stage, and contains photographs and practical work your child has completed. Each term you will be presented with the book which we encourage you to make a comment in the space provided.</p>
<p>If you do not want us to take photographs of your child for inclusion in the book, please let us know in writing.</p>
<p>The book will be given to you when your child leaves pre-school to go to primary school.</p>

<h2 id="parent-involvement">Parent Involvement</h2>
<p>There are several ways you can become involved at Little Owls:</p>
<p>Drop in and play sessions, parent meetings and  in the near future we are looking at having a parents forum.</p>

@stop
