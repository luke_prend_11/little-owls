@extends('layouts.master')
@section('content')

@if($currentSchoolYear)
@include('includes.term-dates', ['schoolYear' => $currentSchoolYear, 'schoolYearHolidays' => $currentSchoolYearHolidays, 'schoolYearBankHolidays' => $currentSchoolYearBankHolidays])
@endif

@if($nextSchoolYear)
@include('includes.term-dates', ['schoolYear' => $nextSchoolYear, 'schoolYearHolidays' => $nextSchoolYearHolidays, 'schoolYearBankHolidays' => $nextSchoolYearBankHolidays])
@endif

@stop
