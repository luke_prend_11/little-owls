@extends('layouts.master')
@section('content')


<h2 id="session-fee">Session Fee</h2>
<ul>
    <li>Fees are £6.00 per hour + £0.60 for snack in the morning and afternoon.</li>
    <li>The session fee is payable for those children who are either not yet in receipt of the Nursery Education Grant or use all their entitlement.</li>
    <li>Fees remain due in the event of your child's absence, for any reason, as our running costs remain the same.</li>
</ul>
<p>Funding for 2 year olds is available the term after their 2nd birthday if you are eligible and meet the criteria for this for further information please contact Family Information Services (01724) 296629.</p>

<h2 id="nursery-education-grant">Nursery Education Grant</h2>
<p>The Nursery Education Grant (NEG) is for 15 hours per week, for 38 weeks of the year, and is applicable for your child from  the term following their third birthday so:</p>

<div class="col-xs-12 col-md-12 no-padding" style="margin-bottom: 30px !important;">
    <div class="staff">
        <span class="col-xs-6 col-sm-6 no-padding">
            <h3>A child born on or between</h3>
        </span>
        <span class="col-xs-6 col-sm-6 no-padding">
            <h3>Will become eligible for a free place from</h3>
        </span>
    </div>
    <div class="staff">
        <span class="col-xs-6 col-sm-6 no-padding role">
            1 April and 31 August
        </span>
        <span class="col-xs-6 col-sm-6 no-padding role">
            1 September following their third birthday
        </span>
    </div>
    <div class="staff">
        <span class="col-xs-6 col-sm-6 no-padding role">
            1 September and 31 December
        </span>
        <span class="col-xs-6 col-sm-6 no-padding role">
            1 January following their third birthday
        </span>
    </div>
    <div class="staff">
        <span class="col-xs-6 col-sm-6 no-padding role">
            1 January and 31 March
        </span>
        <span class="col-xs-6 col-sm-6 no-padding role">
            1 April following their third birthday
        </span>
    </div>
</div>

<h2 id="additional-charges">Additional Charges</h2>
<ul>
    <li>We charge £0.60 per child per session for snack time.</li>
    <li>Fees remain due in the event of your child's absence, for any reason, as our running costs remain the same.</li>
    <li>A provision enhancement charge of £6.00 per month is charged on each invoice.</li>
</ul>

<h2 id="payment-terms">Payment Terms</h2>
<ul>
    <li>Fees must be paid <u>either</u> weekly or by the 15th of the month</li>
        <ul>
            <li><u>Weekly payment</u> – the weekly amount shown on the fees invoice is payable every week.</li>
        </ul>
    <li>Late payment fees after the 15th of each month will incur a charge of £15.00</li>
    <li>Fees remain due in the event of your child's absence, for any reason, as our running costs remain the same.</li>
    <li>If fees are not paid, all additional services offered, other than the free entitlement, will be withdrawn until the fees have been paid.</li>
    <li>Parents will be asked to provide an email address so we can send invoices electronically via Instant Nursery Manager.</li>
    <li>All late or non-payments will be passed to a debt collection agency BFL Solutions who will act on our behalf to retrieve all outstanding payments.</li>
</ul>
<p>In cases of prolonged absence e.g. through illness, parents should consult the Manager/Supervisor.</p>

@stop
