@extends('layouts.master')
@section('content')

<p>Little Owls aims to support children's care and welfare on a daily basis in line with their individual needs. All children need contact with familiar, consistent carers to ensure they can grow confidently and feel self-assured. At times children need to be cuddled, encouraged, held and offered physical reassurance.</p>
<p>Intimate care routines are essential throughout the day to ensure children's basic needs are met. This may include nappy changing, supporting children with toileting, changing clothes where required, first aid treatment and specialist medical support.</p>
<p>In order to maintain the child's privacy, the majority of these actions will take place on a one-to-one basis and wherever possible will be supported by the child's keyworker, with the exception of the first aid treatment that will be conducted by a qualified first aider.</p>
<p>We wish to ensure the safety and welfare of the children involved in intimate care routines and safeguard against any potential harm as well as ensuring the staff member involved is fully supported and able to perform their duties safely and confidently. Through the following actions we will endeavour to support all parties:</p>
<ul>
    <li>Promote consistent and caring relationships through the key person system in the nursery and ensure all parents understand how this works</li>
    <li>Ensure all staff undertaking intimate care routines have suitable enhanced DBS checks</li>
    <li>Train all staff in the appropriate methods for intimate care routines and access specialist training where required, i.e. first aid training, specialist medical support</li>
    <li>Conduct thorough inductions for all new staff to ensure they are fully aware of all nursery procedures relating to intimate care routines</li>
    <li>Follow up on these procedures through supervision meetings and appraisals to identify any areas for development or further training</li>
    <li>Working closely with parents on all aspects of the child's care and education as laid out in the parent and carers as partners policy. This is essential for intimate care routines which require specialist training or support. If a child requires specific support the nursery will arrange a meeting with the parent to discover all the relevant information relating to this to enable the staff to care for the child fully and meet their individual needs</li>
    <li>Ensure all staff have an up-to-date understanding of safeguarding and how to protect children from harm. This will include identifying signs and symptoms of abuse and how to raise these concerns in the most appropriate and speedy manner</li>
    <li>The setting operates a whistleblowing policy as a means for staff to raise concerns relating to their peers. The management will support this by ensuring staff feel confident in raising worries as they arise in order to safeguard the children in the nursery</li>
    <li>The management team regularly conducts working practice observations on all aspects of nursery operations to ensure that procedures are working in practice and all children are supported fully by the staff. This includes intimate care routines</li>
    <li>Staff will be trained in behaviour management techniques which will include using restraint techniques where required, e.g. if a child is likely to hurt themselves or others. Please refer to the nursery behaviour management policy for further information</li>
    <li>The nursery conducts regular risk assessments on all aspects of the nursery operation and this area is no exception. The nursery has assessed all the risks relating to intimate care routines and has placed appropriate safeguards in place to ensure the safety of all involved.</li>
</ul>
<p>If any parent or member of staff has concerns or questions about intimate care procedures or individual routines please see the manager at the earliest opportunity.</p>

@stop
