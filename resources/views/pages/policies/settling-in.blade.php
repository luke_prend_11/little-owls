@extends('layouts.master')
@section('content')

<p>We aim for children to feel safe, stimulated and happy in the nursery and to feel secure and comfortable with all staff. We also want parents to have confidence in both their children's well-being and their role as active partners, with the child being able to benefit from what the nursery has to offer.</p>
<p>We aim to support parents and other carers to help their children settle quickly and easily by giving consideration to the individual needs and circumstances of each child and their families.</p>
<p>The nursery staff will work in partnership with parents to settle their child into the nursery environment by:</p>
<ul>
    <li>Providing parents with relevant information regarding the policies and procedures of the nursery</li>
    <li>Encouraging the parents and children to visit the nursery during the weeks before an admission is planned</li>
    <li>Planning settling in visits and introductory sessions (lasting approximately 1-2 hours). These will be provided free of charge over a one or two week period dependent on individual needs, age and stage of development</li>
    <li>Welcoming parents to stay with their child during the first few weeks until the child feels settled and the parents feel comfortable about leaving their child. Settling in visits and introductory sessions are key to a smooth transition and to ensure good communication and information sharing between staff and parents</li>
    <li>Reassuring parents whose children seem to be taking a long time settling into the nursery</li>
    <li>Encouraging parents, where appropriate, to separate themselves from their children for brief periods at first, gradually building up to longer absences</li>
    <li>Allocating a key person to each child and his/her family, before he/she starts to attend. The key person welcomes and looks after the child and his/her parents during the settling in period, and throughout his/her time at the nursery, to ensure the family has a familiar contact person to assist with the settling in process</li>
    <li>Reviewing the nominated key person if the child is bonding with another member of staff to ensure the child's needs are supported</li>
    <li>Respecting the circumstances of all families, including those who are unable to stay for long periods of time in the nursery and reassure them of their child's progress towards settling in</li>
</ul>

@stop
