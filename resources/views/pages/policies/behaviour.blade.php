@extends('layouts.master')
@section('content')

<h4>EYFS: 3.2, 3.52, 3.53</h4>

<p>At {{ config('constants.SITE_NAME') }} we believe that children flourish best when they know how they and others are expected to behave. Children gain respect through interaction with caring adults who act as good role models, show them respect and value their individual personalities. The nursery actively promotes British values and encourages and praises positive, caring and polite behaviour at all times and provides an environment where children learn to respect themselves, other people and their surroundings.</p>

<p>Children need to have set boundaries of behaviour for their own safety and the safety of their peers. Within the nursery we aim to set these boundaries in a way which helps the child to develop a sense of the significance of their own behaviour, both in their own environment and that of others around them. Restrictions on the child's natural desire to explore and develop their own ideas and concepts are kept to a minimum.</p>

<p>We aim to:</p>
<ul>
    <li>Recognise the individuality of all our children and that some behaviours are normal in young children e.g. biting</li>
    <li>Encourage self-discipline, consideration for each other, our surroundings and property</li>
    <li>Encourage children to participate in a wide range of group activities to enable them to develop their social skills</li>
    <li>Ensure that all staff act as positive role models for children</li>
    <li>Work in partnership with parents by communicating openly</li>
    <li>Praise children and acknowledge their positive actions and attitudes, therefore ensuring that children see that we value and respect them</li>
    <li>Encourage all staff working with children to accept their responsibility for implementing the goals in this policy and to be consistent</li>
    <li>Promote non-violence and encourage children to deal with conflict peacefully</li>
    <li>Provide a key worker system enabling staff to build a strong and positive relationship with children and their families</li>
    <li>Provide activities and stories to help children learn about accepted behaviours, including opportunities for children to contribute to decisions about accepted behaviour where age/stage appropriate</li>
    <li>Supporting and developing self-regulation and empathy as appropriate to stage of development</li>
    <li>Have a named person who has overall responsibility for behaviour management.</li>
</ul>

<p>The named person North: Jo Whiteley, South: Katie Lappin, West: Erika Wilkins for managing behaviour will:</p>

<ul>
    <li>Advise and support other staff on behaviour concerns</li>
    <li>Along with each room leader will keep up to date with legislation and research relating to behaviour</li>
    <li>Support changes to policies and procedures in the nursery</li>
    <li>Access relevant sources of expertise where required and act as a central information source for all involved</li>
    <li>Attend regular external training events, and ensure all staff attend relevant in-house or external training for behaviour management. Keep a record of staff attendance at this training.</li>
</ul>

<p>Our nursery rules are concerned with safety, care and respect for each other. We keep the rules to a minimum and ensure that these are age and stage appropriate.  We regularly involve children in the process of setting rules to encourage cooperation and participation and ensure children gain understanding of the expectations of behaviour relevant to them as a unique child.</p>

<p>Children who behave inappropriately, for example, by physically abusing another child or adult e.g. biting, or through verbal bullying, are helped to talk through their actions and apologise where appropriate. We make sure that the child who has been upset is comforted and the adult will confirm that the other child's behaviour is not acceptable. We always acknowledge when a child is feeling angry or upset and that it is the behaviour that is not acceptable, not the child.</p>

<p>When children behave in unacceptable ways:</p>

<ul>
    <li>We never use or threaten to use physical punishment/corporal punishment such as smacking or shaking </li>
    <li>We only use physical intervention for the purpose of averting immediate danger or personal injury to any person (including the child) or to manage a child’s behaviour if absolutely necessary. We keep a record of any occasions where physical intervention is used and inform parents on the same day, or as reasonably practicable</li>
    <li>We recognise that there may be times where children may have regular occasions where they lose control and may need individual techniques to restrain them. This will only be carried out by staff who have been appropriately trained to do so. Any restraints will only be done following recommended guidance and training and only with a signed agreement from parents on when to use it. We will complete an incident form following any restraints used and notify the parents </li>
    <li>We do not single out children or humiliate them in any way. Where children use unacceptable behaviour they will, wherever possible, be re-directed to alternative activities. Discussions with children will take place as to why their behaviour was not acceptable, respecting their level of understanding and maturity</li>
    <li>Staff will not raise their voices (other than to keep children safe)</li>
    <li>In any case of misbehaviour, we always make it clear to the child or children in question, that it is the behaviour and not the child that is unwelcome</li>
    <li>We decide how to handle a particular type of behaviour depending on the child’s age, level of development and the circumstances surrounding the behaviour. This may involve asking the child to talk and think about what he/she has done. All staff support children in developing empathy and children will only be asked to apologise if they have developed strong empathy skills and have a good understanding of why saying sorry is appropriate </li>
    <li>We help staff to reflect on their own responses towards behaviours that challenge to ensure that their reactions are appropriate</li>
    <li>We inform parents if their child’s behaviour is unkind to others or if their child has been upset. In all cases we deal with behaviour that challenges in nursery at the time. We may ask parents to meet with staff to discuss their child's behaviour, so that if there are any difficulties we can work together to ensure consistency between their home and the nursery. In some cases we may request additional advice and support from other professionals, such as an educational psychologist</li>
    <li>We support children in developing non-aggressive strategies to enable them to express their feelings</li>
    <li>We keep confidential records on any behaviour that challenges that has taken place. We inform parents and ask them to read and sign any incidents concerning their child</li>
    <li>We support all children to develop positive behaviour, and we make every effort to provide for their individual needs</li>
    <li>Through partnership with parents and formal observations, we make every effort to identify any behavioural concerns and the causes of that behaviour. From these observations and discussions, we will implement an individual behaviour modification plan where a child’s behaviour involves aggressive actions towards other children and staff, for example hitting, kicking etc. The manager will complete risk assessments identifying any potential triggers or warning signs ensuring other children’s and staff’s safety at all times. In these instances we may remove a child from an area until they have calmed down. </li>
</ul>

<h2 id="antibullying">Anti-bullying</h2>

<p>Bullying takes many forms. It can be physical, verbal or emotional, but it is always a repeated behaviour that makes other people feel uncomfortable or threatened.  We acknowledge that any form of bullying is unacceptable and will be dealt with immediately while recognising that physical aggression is part of children’s development in their early years.</p>

<p>We recognise that children need their own time and space and that it is not always appropriate to expect a child to share. We believe it is important to acknowledge each child’s feelings and to help them understand how others might be feeling.</p>

<p>We encourage children to recognise that bullying, fighting, hurting and discriminatory comments are not acceptable behaviour. We want children to recognise that certain actions are right and that others are wrong.</p>

<p>At our nursery, staff follow the procedure below to enable them to deal with behaviour that challenges:</p>

<ul>
    <li>Staff are encouraged to ensure that all children feel safe, happy and secure</li>
    <li>Staff are encouraged to recognise that active physical aggression in the early years is part of the child’s development and that it should be channelled in a positive way</li>
    <li>Children are helped to understand that using aggression to get things, is inappropriate and they will be encouraged to resolve problems in other ways</li>
    <li>Our staff will intervene when they think a child is being bullied, however mild or harmless it may seem</li>
    <li>Staff will initiate games and activities with children when they feel play has become aggressive, both indoors or out</li>
    <li>Staff will sensitively discuss any instance of bullying with the parents of all involved to look for a consistent resolution to the behaviour</li>
    <li>We will ensure that this policy is available for staff and parents and it will be actively publicised at least once a year to parents and staff</li>
    <li>If any parent has a concern about their child, a member of staff will be available to discuss those concerns. It is only through co-operation that we can ensure our children feel confident and secure in their environment, both at home and in the nursery</li>
    <li>All concerns will be treated in the strictest confidence.</li>
</ul>

<p>By positively promoting good behaviour, valuing co-operation and a caring attitude, we hope to ensure that children will develop as responsible members of society. </p>

<table class="table">
  <thead>
    <tr>
      <th scope="col">This policy was adopted on</th>
      <th scope="col">Signed on behalf of the nursery</th>
      <th scope="col">Date for review</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">September 2019</th>
      <td>K Clapson</td>
      <td>September 2020</td>
    </tr>
  </tbody>
</table>

@stop
