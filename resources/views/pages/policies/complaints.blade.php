@extends('layouts.master')
@section('content')

<h2 id="rationale">Rationale</h2>
<p>{{ config('constants.SHORT_SITE_NAME') }} aims to provide the highest quality care and education for children attending the Pre-School by holding the principles of the Helping Children Achieve More at the heart of its work. We will endeavour to ensure children are happy during their time at {{ config('constants.SHORT_SITE_NAME') }} and that parents and carers are pleased and satisfied with the quality of the Pre-School. {{ config('constants.SHORT_SITE_NAME') }} will listen and take seriously any concerns or issues raised by parents and take appropriate steps to resolve any problems.</p>
<p>{{ config('constants.SHORT_SITE_NAME') }} has written this policy to ensure that best practice and procedures are carried out at the Pre-School. This policy complies with the legal requirements of the Early Years Foundation Stage statutory framework.</p>

<h2 id="implementation">Implementation</h2>
<p>{{ config('constants.SHORT_SITE_NAME') }} members of staff will be responsible for managing complaints on a daily basis. If a complaint is made against a member of staff or key worker, the Pre-School Manager will conduct the investigation. All complaints made to staff will be recorded in detail in the Incident Record Folder using an Incident Log Report Sheet.</p>
<p>This policy constitutes the Pre-School's formal Complaints Procedure and will be available from the Pre-School Manager upon request.</p>

<h2 id="stage-one">Stage One</h2>
<ul>
    <li>In the event of a complaint by a parent or carer regarding an aspect of the Pre-School's work or about a member of staff, it should in most cases be possible to resolve the problem by discussing the situation with the individual concerned and coming to a mutually agreed solution.</li>
    <li>The Pre-School is committed to an open door policy with parents and welcomes comments about quality of the Pre-School and services. Any negative points will be acted upon and necessary actions agreed and carried out immediately.</li>
    <li>In the first instance of a concern arising, parents and carers are required to speak directly with the relevant member of staff; if it is not viable to speak to the member of staff the Pre-School Manager should be consulted. The Pre-School Manager will seek to resolve the problem with the parent in a calm and professional manner. If the situation is not resolved to the satisfaction of the complainant Stage Two of the procedure will come into operation.</li>
</ul>

<h2 id="stage-two">Stage Two</h2>
<ul>
    <li>If Stage 1 procedures have failed to produce a resolution to the complainant should put the issues in writing to the managers of the pre-school.</li>
    <li>{{ config('constants.SHORT_SITE_NAME') }} will acknowledge receipt of the complaint within three working days in writing to the complainant. The complaint will be fully investigated within 10 working days and a written reply sent to the person making the complaint. If an unforeseen delay occurs, the Pre-School will advise the parent or carers of this and offer an apology and date for an expected reply and resolution.</li>
    <li>If {{ config('constants.SHORT_SITE_NAME') }} believes that the issue has Child Protection implications, they must inform the designated Child Protection Office immediately according to the procedure set out in the Child Protection Policy.</li>
    <li>If any party involved in the complaint has good reason to believe that a criminal offence has been committed, then have a legal obligation and responsibility to contact the police.</li>
    <li>The written response to the complaint from will be sent to the parent and carer concerned and copied to all relevant members of staff or implicated during the investigation. The response will include the conclusion to the full investigation and any amendments to the Pre-School's policies, practices or procedures to prevent the situation arising in the future.</li>
    <li>The Pre-School Manager will offer to meet the parent or carer concerned to discuss the complaint and the Pre-School's investigation and conclusion.</li>
    <li>At all times the Pre-School and Management will seek to re-establish a positive and constructive relationship with the complainant.</li>
</ul>

<h2 id="making-a-complaint-to-ofsted">Making a Complaint to OFSTED</h2>
<p>If a parent or carer does not feel that the investigation satisfactorily answered their complaint they can submit a complaint to OFSTED.</p>
<p>The OFSTED regulator for {{ config('constants.SHORT_SITE_NAME') }} is:</p>
<p><strong>Ofsted</strong><br />
Piccadilly Gate<br />
Store Street<br />
Manchester<br />
M1 2WD<br />
0300 123 4666</p>
<p>Any complaints received will be kept in a Complaints Record file kept on the Pre-School premises and will be reviewed annually by the Managers. Any questions about this policy should be directed to the Pre-School Manager Sharon Holtby.</p>

@stop
