@extends('layouts.master')
@section('content')

<h4>EYFS: 3.47 – 3.49</h4>

<p>At {{ config('constants.SHORT_SITE_NAME') }} we believe that mealtimes should be happy, social occasions for children and staff alike. We promote shared, enjoyable positive interactions at these times.</p>

<p>We are committed to offering children healthy, nutritious snacks which meet individual needs and requirements.</p>

<p>We will ensure that:</p>
<ul>
    <li>A balanced and healthy two daily snacks are provided for children attending a full day at the nursery</li>
    <li>Parents are encouraged to provide their children with healthy eating lunch boxes</li>
    <li>Menus are planned in advance, rotated regularly and reflect cultural diversity and variation. These are displayed for children and parents to view</li>
    <li>We provide nutritious food at all snack times, avoiding large quantities of fat, sugar, salt and artificial additives, preservatives and colourings</li>
    <li>Fresh drinking water is always available and accessible. It is frequently offered to children and intake is monitored. In hot weather staff will encourage children to drink more water to keep them hydrated</li>
    <li>Individual dietary requirements are respected. We gather information from parents regarding their children’s dietary needs, including any special dietary requirements, preferences and food allergies that a child has and any special health requirements, before a child starts or joins the nursery. Where appropriate, we will carry out a risk assessment in the case of allergies and work alongside parents to put into place an individual dietary plan for their child</li>
    <li>We give careful consideration to seating to avoid cross contamination of food from child to child. Where appropriate, an adult will sit with children during meals to ensure safety and minimise risks. Where appropriate, age/stage discussions will also take place with all children about allergies and potential risks to make them aware of the dangers of sharing certain foods </li>
    <li>Staff show sensitivity in providing for children’s diets and allergies. They do not use a child’s diet or allergy as a label for the child, or make a child feel singled out because of her/his diet or allergy</li>
    <li>Staff set a good example and eat with the children and show good table manners. Meal and snack times are organised so that they are social occasions in which children and staff participate in small groups. During meals and snack times children are encouraged to use their manners and say 'please' and 'thank you' and conversation is encouraged </li>
    <li>Staff use meal and snack times to help children to develop independence through making choices, serving food and drink, and feeding themselves</li>
    <li>Staff support children to make healthy choices and understand the need for healthy eating</li>
    <li>We provide foods from the diet of each of the children’s cultural backgrounds, providing children with familiar foods and introducing them to new ones.</li>
    <li>Cultural differences in eating habits are respected</li>
    <li>Any child who shows signs of distress at being faced with a meal he/she does not like will have his/her food removed without any fuss. If a child does not finish his/her first course, he/she will still be given a helping of dessert</li>
    <li>Children not on special diets are encouraged to eat a small piece of everything</li>
    <li>Children who refuse to eat at the mealtime are offered food later in the day</li>
    <li>Children are given time to eat at their own pace and not rushed</li>
    <li>Quantities offered take account of the ages of the children being catered for in line with recommended portion sizes for  young children</li>
    <li>We promote positive attitudes to healthy eating through play opportunities and discussions</li>
    <li>No child is ever left alone when eating/drinking to minimise the risk of choking</li>
    <li>We will sometimes celebrate special occasions such as birthdays with the occasional treat of foods such as cake, sweets or biscuits. These will be given at mealtimes to prevent tooth decay and not spoil the child’s appetite. Where we have frequent birthdays and celebrations we consider other alternatives such as celebrating through smiles and praise, stickers and badges, choosing a favourite story, becoming a special helper, playing a party game, dancing and/or singing their favourite song</li>
    <li>We do allow parents to bring in cakes on special occasions. We ensure that all food brought in from parents meets the above and health and safety requirements and ingredients that are listed within the Food Information for Consumers (FIR) 2014 and detailed in the allergens policy and procedure</li>
    <li>All staff who prepare and handle food are competent to do so and receive training in food hygiene which is updated every three years</li>
    <li>In the very unlikely event of any food poisoning affecting two or more children on the premises, whether or not this may arise from food offered at the nursery, we will inform Ofsted as soon as reasonably practical and in all cases within 14 days. We will also inform the relevant health agencies and follow any advice given.</li>
</ul>

<table class="table">
  <thead>
    <tr>
      <th scope="col">This policy was adopted on</th>
      <th scope="col">Signed on behalf of the nursery</th>
      <th scope="col">Date for review</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">September 2019</th>
      <td>K Clapson</td>
      <td>September 2020</td>
    </tr>
  </tbody>
</table>

@stop
