@extends('layouts.master')
@section('content')

<p>This policy is updated and revised annually in line with the revised Code of Practice.</p>
<p>Our pre-school/nursery promotes a positive self-image to all children and respects their individuality, planning for all children according to their needs, irrespective of their gender, race, religion or ability. We ensure we have non stereotypical images in order to help overcome preconceived ideas of gender, ethnic origin, culture or religion. Every child is included and not disadvantaged because of home language, culture or religion, family background, learning difficulties or disability. We provide books, materials and equipment that are multicultural and non-sexist, and positive images of all groups including the disabled. We provide a welcoming environment that will offer support and guidance for every child and their carer. We aim to develop children' s positive self-esteem and the esteem of others and will set a good example by treating one another and the children with respect.</p>
<p>The legal frameworks for this policy include:</p>
<ul>
    <li>Education Act 1996</li>
    <li>Special Educational Needs and Disability Code of Practice 2014</li>
    <li>Special Education Needs and Disability Regulations 2014</li>
    <li>Children and Families Act 2014</li>
    <li>Care Act 2014</li>
    <li>The Statutory Framework for the Early Years Foundation Stage</li>
    <li>Safeguarding Disabled Children-practice guidance 2009</li>
    <li>Children Act 1989</li>
    <li>Childcare Act 2006</li>
    <li>Equality Act 2010</li>
</ul>

<h2 id="admissions">Admissions</h2>
<p>Children with special/additional needs, like other children are admitted to pre-school after consultation between parents/carers, pre-school management and outside agencies. We ask parents to give as much notice as possible for a child with disabilities or special/additional needs, to enable us to explore how we can provide most effectively for that child, this may include staffing ratios being affected, therefore each individual case is discussed between families and management in order to enable a smooth transition from home to our nursery. Where a request is initiated by an outside agency, the request will be considered in line with our Admissions Policy. All children are welcome, information on the admission form and at the initial parents meeting is collected to ensure each child's individual needs are met. In some cases we may seek further professional advice to support the admission of a child with special/additional needs to ensure adequate and suitable provision can be made to suit their needs.</p>
<p>In order to promote Equality and Diversity we will:</p>
<ul>
    <li>Ensure that all parents are made aware of our policy</li>
    <li>Offer equality and choice for all</li>
    <li>Access additional funding where necessary and available</li>
    <li>Reflect the diversity of members of our society in our publicity and promotional materials</li>
    <li>Ensure our admissions policy promotes equality for all families</li>
    <li>Not discriminate against a family or prevent admission to our setting on any grounds</li>
    <li>Provide opportunities for parents/carers to contribute to their child's care and education</li>
    <li>Use funding from the pre-school budget, where possible and as the budget allows, using for special educational needs including training</li>
    <li>Challenge inappropriate attitudes and practices by staff children and parent/carers</li>
</ul>
<p>We aim to encourage children to develop positive attitudes about themselves and other people. We will do this by:</p>
<ul>
    <li>Listening to children and ensuring each child feels included, safe, valued and respected.</li>
    <li>Ensuring that all children have equal access to activities, resources and learning opportunities.</li>
    <li>Making appropriate provision to ensure each child receives the widest possible opportunity to develop their skills and abilities and recognise different learning styles.</li>
    <li>Providing play materials/resources and activities that demonstrates diversity of background and ability, and help to develop positive attitudes to differences of race, culture, language, gender and ability.</li>
    <li>Avoiding stereotypical images in equipment, resources and activities.</li>
    <li>Using positive non-discriminatory language with all children.</li>
    <li>Valuing the home background of all children.</li>
</ul>

<p>In order to meet the children's diverse needs we will endeavour to plan a wide range of challenging play opportunities to develop their knowledge and skills, develop motivation, self-esteem and concentration, provide a safe and supportive environment and provide support by different approaches, including additional adult help or other agencies where appropriate.</p>
<p>We are primarily concerned with identifying/planning to meet individual needs, providing a well-planned structured environment with access available for all children, providing a wide range of activities, resources and equipment, giving access to different types and levels of interaction and communication, identifying any specialist skills, methods or strategies, evaluating and recording individual learning and achievements.</p>

<h2 id="english-as-an-additional-language">English as an Additional Language</h2>
<p>We will value linguistic diversity and seek support for children and parents as required. We understand that young bilingual learners need time to observe, tune into the new language and try out things that are unfamiliar to them.</p>
<p>The setting will try to provide information in languages that reflect the needs of our families who speak English as an additional language.</p>
<p>Alongside valuing parent's home language, we will provide a range of meaningful contexts in which children have opportunities to develop English.  English will be crucial as the language they use to access learning.</p>

<h2 id="inclusion">Inclusion</h2>
<p>We aim to provide a happy stimulating and secure environment for all children regardless of culture, background or disability, where individual abilities are recognised and children learn through first hand experiences, exploration, practice and discovery. Each person is regarded as an individual, with differing social, intellectual and cultural backgrounds. Different needs, likes, dislikes, similarities and differences are respected and accounted for. Nobody in the pre-school is subjected to discrimination, racist comments or gender bias. Cultural and religious diversity is respected. Inclusion is not optional, children have defined entitlements in this area and settings have legal responsibilities. During play we encourage the children to respect and value each other. We discourage them from making hurtful and unkind remarks. We challenge any discriminatory actions or comments made by staff or children. We acknowledge the diversity of our society and help prepare the children for their part in society. Staffs ensure that children are helped towards understanding that it is wrong to judge someone because of their gender, race, beliefs, disability or social background. We explain why, talk things through, making it clear immediately the unacceptability of the behaviour and attitudes, by means of explanation rather than personal blame, and praise positive behaviour. We aim to treat our children with equal respect and provide a range of equipment resources and activities to meet their individual needs. We seek to respond to each child as an individual. We believe in adapting our practice to meet the needs of each child rather than just making children fit in with what we do. We work together with parents and professionals valuing their experience and contribution. We also liase with the Local Authority early years representative (Val Taylor).</p>
<p>We comply with the two main duties set out in the EQUALITY ACT 2010. These are:</p>
<ul>
    <li>Not to treat a disabled child 'less favourably'</li>
    <li>To make 'reasonable adjustments' for disabled children.</li>
</ul>

<p>We will focus on each child's individual learning, development and care by:</p>
<ul>
    <li>Removing or helping to overcome barriers for children where these already exist.</li>
    <li>Being alert to the early signs of needs that could lead to later difficulties and responding quickly and appropriately, involving other agencies as necessary.</li>
    <li>Stretching and challenging all children.</li>
</ul>

<h2 id="staff-training">Staff Training</h2>
<p>We have the opportunity to access termly training and updates from the Early Years Team.</p>
<p>Staff will be encouraged to attend training opportunities to support their awareness and understanding of equality and diversity.</p>
<p>The setting SENCO will attend training around special educational needs and the code of practice.</p>
<p>Staff will have equal access to identified training to ensure professional development.</p>
<p>Funding will be made available (where possible), from within the pre-school budget to allow staff to access training. Currently, 2 free places are available for SENCO's to access the termly updates.</p>
<p>Any complaints will be dealt with in accordance with our complaints policy.</p>
<p>We will regularly review and monitor this policy and our practice to ensure that we are fully implementing the policy for Equality of Opportunities and SEN. We will continue to monitor the effectiveness of the policies and update annually or as and when deemed necessary.</p>

<h2 id="sen-policy">SEN Policy</h1>
<p>This policy is updated and revised annually with the revised code of practice.</p>
<p>We apply the SEN Code of Practice to ensure that children with Special Educational Needs and their families are welcomed and fully inclusive in all aspects of Pre-school life.</p>
<p>The legal frameworks for this policy include:</p>
<ul>
    <li>Working Together to Safeguard Children 2013</li>
    <li>Special Educational Needs and Disability Code of Practice 2014</li>
    <li>Special Educational Needs and Disability Regulations 2014</li>
    <li>Children and Families Act 2014</li>
    <li>The Statutory Framework for the Early Years Foundation Stage</li>
    <li>Equality Act 2010</li>
</ul>

<p>We will identify children who give cause for concern as early as possible by observing and assessing the child's stage of development, gathering information, recording progress made, interacting with the child and discussion with parents/carers to ensure they make the maximum progress possible.</p>
<p>We will seek support and advice from parents, external agencies and other relevant professionals to ensure that relevant background information about the children is collected, recorded and updated and that we are offering effective provision and appropriate targets for children with additional needs.</p>

<h3>Graduated Approach Cycle of Action</h3>
<p>We will consult with parents and complete an Early Identification Assessment Summary form to start the cycle of Assess, Plan, Do, Review, to address any learning and development needs. We will implement and record development targets and strategies over a period of time to enable the child to progress.</p>

<p>This will include information about:</p>
<ul>
    <li>Small steps targets for the child. They will be written as SMART targets: Specific, Measurable, Achievable, Realistic and Timed.</li>
    <li>Strategies to be used</li>
    <li>The provision to be put in place</li>
    <li>Review date</li>
    <li>Outcome of action to be taken</li>
    <li>Set targets in partnership with parents and a review date.</li>
    <li>Record progress with targets and review with parents to decide on next stage of action.</li>
</ul>
<p>During this cycle it may be considered that a referral may be needed to another professional or specialist. Once a specialist becomes involved we would work with them to write joint targets or shared outcome plans to support the development of the child. All records are kept strictly confidential and used only on a need to know basis. Our records will be updated regularly with the full knowledge and support of parents.</p>

<p>Children with special/additional needs are defined as having a learning difficulty if they have:</p>
<ul>
    <li>A significantly greater difficulty in learning than the majority of children of the same age.</li>
    <li>Have a disability that prevents or hinders them from making use of educational facilities of a kind generally provided for children of the same age in schools within the area of the local education authority.</li>
</ul>
<p>Children must not be regarded as having a learning difficulty solely because the language or form of language of their home is different from the language in which they will be taught. (Ref: SEN Code of Practice)</p>
<p>We will work together with parents as partners, they have unique knowledge and experience to give about their children, and we will value the contribution they make. Parental agreement will always be made before any contact is made with any professional outside our setting.</p>
<p>Parents will always be involved in any discussions made in relation to educational provision and arrangements made to meet their child's needs. Parents will be kept fully informed of their child's progress including any cause for concern. Where necessary we will liase and work with relevant outside agencies and professionals in order to meet the child's specific needs. We will aim to provide a range of suitable learning opportunities to support structured programmes already in place by other agencies. We welcome any advice, support and involvement from any external agency that may be involved with children in our care.</p>
<p>Advice and guidance could be sought from:</p>
<ul>
<li>ISDC Early Years Inclusion Team (previously Portage service)</li>
<li>Education Preparation unit</li>
<li>Speech and Language Therapy</li>
<li>Hearing Support Service</li>
<li>Visual Impairment Team</li>
<li>Health Visitors</li>
<li>Educational Psychology</li>
<li>Autism Spectrum Education Team.</li>
<li>The Cycle of the Graduated Approach would then continue.</li>
</ul>

<h3>Education, Health and Care Plans.</h3>
<p>In conjunction with parents we take advice from and follow the agency with regard to making a referral for a formal assessment and procedures relating to the child having an Education, Health and Care plan. During this period of time we would continue to gather evidence and continue to gather evidence and continue to implement targets or outcome plans.</p>
<p>On transition all SEN information will be passed to the receiving setting with the knowledge of the parents, using the North Lincolnshire Pre-school to school SEN transfer form in conjunction with the standard Mapping and sharing book and school transfer document.</p>
<p>The names of our Special Educational Needs Co-ordinators (SENCO's) are; Joanne Whiteley (North), Katie Lappin (South), Erika Wilkins (West).</p>
<p>The SENCO will;</p>
<ul>
    <li>Advise and support other practitioners in the setting.</li>
    <li>Ensure I.E.P's are in place in respect of children with additional needs.</li>
    <li>Promote effective relationships with parents of children with additional needs.</li>
    <li>Ensure the Equality of Opportunity and SEN policies are put into practice.</li>
    <li>Ensure staff understand, are familiar with and follow the practice as stated in this policy.</li>
    <li>Establish a record of SEN and ensure it is kept up to date.</li>
    <li>Act as a link with parents, external and support agencies.</li>
    <li>Promote staff development and ensure appropriate training for SEN/Inclusion.</li>
    <li>Monitor the Equality of Opportunity and SEN policies and regularly evaluate and review.</li>
    <li>Liase regularly with the LA SENCO Team and SEN/Inclusion team members through meetings, courses, on an individual basis and by invitation to the setting when specific advice and support have been requested.</li>
</ul>

<p>The SENCO's will attend LA training courses to extend their SEN knowledge and keep up to date with current SEN issues and Code of Practice for their professional development and will address training issues in relation to SEN for other members of staff as appropriate. </p>
<p>The Looked after Children Co-ordinators are the SENCO's at each setting. The co-ordinators will liase with the appropriate professionals to set targets as and when needed with regard to Looked After Children, to help focus for their individual needs and development.</p>
<p>Any complaints will be dealt with in accordance with our complaints policy.</p>
<p>We will regularly review and monitor this policy and our practice to ensure that we are fully implementing the policy for Equality of Opportunities and SEN. We will continue to monitor the effectiveness of the policies and update annually or as and when deemed necessary.</p>

@stop
