@extends('layouts.master')
@section('content')

<p>When dealing with medication of any kind in the nursery, strict guidelines will be followed.</p>

<h2 id="prescription-medication">Prescription medication</h2>
<ul>
    <li>Prescription medicine will only be given to the person named on the bottle for the dosage stated</li>
    <li>Medicines must be in their original containers</li>
    <li>Those with parental responsibility of any child requiring prescription medication should allow a senior member of staff to have sight of the bottle. The staff member should note the details of the administration on the appropriate form and another member of staff should check these details</li>
    <li>Those with parental responsibility must give prior written permission for the administration of each and every medication. However we will except written permission once for a whole course of medication or for the ongoing use of a particular medication under the following circumstances:</li>
    <ol>
        <li>The written permission is only acceptable for that brand name of medication and cannot be used for similar types of medication, e.g. if the course of antibiotics changes, a new form will need to be completed</li>
        <li>The dosage on the written permission is the only dosage that will be administered. We will not give a different dose unless a new form is completed</li>
        <li>Parents should notify us IMMEDIATELY if the child's circumstances change, e.g. a dose has been given at home, or a change in strength/dose needs to be given.</li>
    </ol>
    <li>The nursery will not administer a dosage that exceeds the recommended dose on the instructions unless accompanied by a doctor's letter</li>
    <li>The parent must be asked when the child had last been given the medication before coming to nursery; this information will be recorded on the medication form. Similarly when the child is picked up, the parent or guardian must be given precise details of the times and dosage given throughout the day. The parent's signature must be obtained at both times</li>
    <li>At the time of administering the medicine, a senior member of staff will ask the child to take the medicine, or offer it in a manner acceptable to the child at the prescribed time and in the prescribed form. (It is important to note that staff working with children are not legally obliged to administer medication)</li>
    <li>If the child refuses to take the appropriate medication then a note will be made on the form</li>
    <li>Where medication is "essential" or may have side effects, discussion with the parent will take place to establish the appropriate response</li>
    <li>Wherever possible ask parents to request that GPs prescribe the least number of doses per day, i.e. three times daily, rather than four times daily.</li>
</ul>

<h2 id="nonprescription-medication">Non-prescription medication</h2>
<p>Little Owls will not administer non-prescription medication.</p>

<h2 id="staff-medication">Staff medication</h2>
<p>The first aid box for staff should be kept in a readily accessible position, but out of reach of the children.</p>
<p>First aid boxes should only contain items permitted by the Health and Safety (First Aid) Regulations Act 1981, such as sterile dressing, bandages, and eye pads. No other medical items, such as paracetamol should be kept in the first aid box.</p>

<h2 id="storage">Storage</h2>
<p>All medication for children must have the child's name clearly written on the original container and kept in a closed box, which is out of reach of all children and under supervision at all times. If this box is left unguarded at anytime throughout the day, we have a procedure in place to ensure the safety of any child or adult in the nursery, including visitors, parents and siblings able to access the area.</p>
<p>Emergency medication, such as inhalers and epipens, will be within easy reach of staff in case of an immediate need, but will remain out of children's reach and under supervision at all times.</p>
<p>Any antibiotics requiring refrigeration must be kept in an area inaccessible to children.</p>

@stop
