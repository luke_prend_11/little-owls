@extends('layouts.master')
@section('content')

<p>{{ config('constants.SHORT_SITE_NAME') }} is registered for children between the ages of 2 years and 8 years.</p>
<p>The above statement is taken from the registration document and is the overriding policy in respect of admissions.</p>
<p>Other matters taken into account in deciding which child can be offered a place in the nursery are:</p>
<ul>
    <li>Availability of places, taking into account the staff/child ratios, the age of the child and the registration requirements</li>
    <li>Children who have siblings who are already with us</li>
    <li>When the application is received (extra weight is given to those who have been on the waiting list the longest)</li>
    <li>The nursery's ability to provide the facilities for the welfare of the child, including appropriate staffing arrangements</li>
    <li>Extenuating circumstances affecting the child's welfare or the welfare of his/her family.</li>
</ul>
<p>We operate an inclusion and equality policy and ensure that all children have access to nursery places and services irrespective of their gender, race, disability, religion or belief or sexual orientation of parents.</p>
<p>Prior to a child attending nursery, parents must complete and sign a contract and registration form.  These forms provide the nursery with personal details relating to the child. For example, name, date of birth, address, emergency contact details, parental responsibilities, dietary requirements, collection arrangements, fees and sessions, contact details for parents, doctor's contact details, health visitor contact details, allergies, parental consent and vaccinations etc.</p>

<h2 id="providers-eligible-to-provide-government-funded-places--england">Providers eligible to provide government funded places – England</h2>
<p>All settings registered to accept government funding (detailed in the code of practice) must offer free places for two/three to five year olds for the sessions specified by the local authority. At Little Owls we currently provide free funded places available for children subject to availability. These places will be allocated on a first-come, first-served basis and can be booked a term in advance. Please note for admissions for the free nursery education we have a termly intake, beginning the term following your child's second/third birthday.</p>
<p>All funded sessions are now in line with the flexible arrangement as specified by the Government. When you register your child for their funded place we will discuss your needs, and as far as possible with availability and staffing arrangements we will accommodate your wishes.</p>

@stop
