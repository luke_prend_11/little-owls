@extends('layouts.master')
@section('content')

<h2 id="the-meaning-of-transition">The Meaning of Transition</h2>
<p>Transition concerns the change a child encounters from one place to another. As children develop from birth throughout childhood they move, or transition, from one learning environment or setting to a new one. Often, these transitions involve a process of change that requires them to adapt their thoughts, feelings and behaviours to meet new expectations. By the time a child reaches school age they may have already experienced several transition periods including:</p>
<ul>
    <li>The transition from home to the setting</li>
    <li>The transition between room bases</li>
    <li>The transition from one provider to another during the working week</li>
    <li>The transition from a childcare provider to a school</li>
</ul>
<p>Transition is like a journey and takes time, preparation and planning. Adults can help a child's journey into new territory by supporting them before, during and after the transition occurs. Parents and Practitioners need to work together, sharing information they have about the child and what support he or she may need.</p>

<h2 id="rationale">Rationale</h2>
<p>We believe all children should feel as comfortable, confident and emotionally secure as possiblewhen entering the early years setting. Young children starting a nursery need support to enable them to adjust to a temporary separation from their family. They need to feel that they are a valuable, competent member of the new social group and they need to develop positive attitudes towards the range of new experiences they will encounter. During the transition children need to be helped to retain the self confidence and self respect that they have already gained at home or in previous settings. Getting to know a child and planning for admission to the setting requires parents and practitioners to engage in an equal partnership in which both learn from each other.</p>

<h2 id="our-aim">Our Aim</h2>
<p>Transitions involve a process of change that require a period of adjustment for you and your child. To help your family adapt to change, we aim to provide as much continuity of education and care as possible. We will accomplish this by sharing information about your child and working closely with you, other providers who care for your child and any provider your child may move on to. (where parents are referred to in this and other policies, this also includes carers, child minders etc.)</p>

<h2 id="transition-from-home-to-the-setting">Transition from Home to the Setting</h2>
<ul>
    <li>Information sharing (learning journey) is the child's starting point</li>
    <li>Settling in procedures</li>
    <li>Role of key person</li>
    <li>Comforters / objects of reference</li>
    <li>Parents pack / outline of routine / policies</li>
</ul>

@stop
