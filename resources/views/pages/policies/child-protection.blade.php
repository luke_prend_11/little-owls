@extends('layouts.master')
@section('content')

<p>Disclaimer from Ofsted: The EYFS requires that a setting's safeguarding policy 'should be in line with the guidance and procedures of the relevant local authority'.</p>

<h4>EYFS: 3.4-3.18, 3.19, 3.21, 3.22</h4>

<p>At Little Owls Nursery we work with children, parents, external agencies and the community to ensure the welfare and safety of children and to give them the very best start in life. Children have the right to be treated with respect, be helped to thrive and to be safe from any abuse in whatever form.</p>

<p>We support the children within our care, protect them from maltreatment and have robust procedures in place to prevent the impairment of children’s health and development. In our setting we strive to protect children from the risk of radicalisation and we promote acceptance and tolerance of other beliefs and cultures (please refer to our inclusion and equality policy for further information). Safeguarding is a much wider subject than the elements covered within this single policy, therefore this document should be used in conjunction with the nursery’s other policies and procedures.</p>

<p>This policy works alongside these other specific policies to cover all aspects of child protection:</p>

<ul>
    <li>Online safety</li>
    <li>Human Trafficking and Modern Slavery</li>
    <li>Prevent Duty and Radicalisation</li>
    <li>Domestic Violence, Honour Based Violence (HBV) and Forced Marriages </li>
    <li>Looked After Children</li>
</ul>

<p>Legal framework and definition of safeguarding</p>
<ul>
    <li>Children Act 1989 and 2004</li>
    <li>Childcare Act 2006</li>
    <li>Safeguarding Vulnerable Groups Act 2006</li>
    <li>Children and Social Work Act 2017</li>
    <li>The Statutory Framework for the Early Years Foundation Stage (EYFS) 2021</li>
    <li>Working together to safeguard children 2018</li>
    <li>Keeping children safe in education 2019</li>
    <li>Data Protection Act 2018</li>
    <li>What to do if you’re worried a child is being abused 2015</li>
    <li>Counter-Terrorism and Security Act 2015.</li>
</ul>


<p>Safeguarding and promoting the welfare of children, in relation to this policy is defined as: </p>
<ul>
    <li>Protecting children from maltreatment</li>
    <li>Preventing the impairment of children’s health or development</li>
    <li>Ensuring that children are growing up in circumstances consistent with the provision of safe and effective care</li>
    <li>Taking action to enable all children to have the best outcomes. (Definition taken from the HM Government document ‘Working together to safeguard children 2018).</li>
</ul>

<h2 id="policy-intention">Policy intention</h2>
<p>To safeguard children and promote their welfare we will:</p>
<ul>
    <li>Create an environment to encourage children to develop a positive self-image</li>
    <li>Provide positive role models and develop a safe culture where staff are confident to raise concerns about professional conduct</li>
    <li>Support staff to notice the softer signs of abuse and know what action to take</li>
    <li>Encourage children to develop a sense of independence and autonomy in a way that is appropriate to their age and stage of development</li>
    <li>Provide a safe and secure environment for all children</li>
    <li>Promote tolerance and acceptance of different beliefs, cultures and communities</li>
    <li>Help children to understand how they can influence and participate in decision-making and how to promote British values through play, discussion and role modelling</li>
    <li>Always listen to children</li>
    <li>Provide an environment where practitioners are confident to identify where children and families may need intervention and seek the help they need</li>
    <li>Share information with other agencies as appropriate.</li>
</ul>

<p>The nursery is aware that abuse does occur in our society and we are vigilant in identifying signs of abuse and reporting concerns. Our practitioners have a duty to protect and promote the welfare of children. Due to the many hours of care we are providing, staff may often be the first people to identify that there may be a problem. They may well be the first people in whom children confide information that may suggest abuse or to spot changes in a child’s behaviour which may indicate abuse.</p>

<p>Our prime responsibility is the welfare and well-being of each child in our care. As such we believe we have a duty to the children, parents and staff to act quickly and responsibly in any instance that may come to our attention. This includes sharing information with any relevant agencies such as local authority services for children’s social care, health professionals or the police. All staff will work with other agencies in the best interest of the child, including as part of a multi-agency team, where needed.</p>

<p>The nursery aims to:</p>
<ul>
    <li>Keep the child at the centre of all we do</li>
    <li>Ensure staff are trained right from induction to understand the child protection and safeguarding policy and procedures, are alert to identify possible signs of abuse (including the signs known as softer signs of abuse), understand what is meant by child protection and are aware of the different ways in which children can be harmed, including by other children through bullying or discriminatory behaviour</li>
    <li>Be aware of the increased vulnerability of children with Special Educational Needs and Disabilities (SEND) and other vulnerable or isolated families and children</li>
    <li>Ensure that all staff feel confident and supported to act in the best interest of the child, share information and seek the help that the child may need</li>
    <li>Ensure that all staff are familiar and updated regularly with child protection training and procedures and kept informed of changes to local/national procedures, including thorough annual safeguarding newsletters and updates</li>
    <li>Make any child protection referrals in a timely way, sharing relevant information as necessary in line with procedures set out by North Lincolnshire Children’s Services Duty Team.</li>
    <li>Ensure that information is shared only with those people who need to know in order to protect the child and act in their best interest</li>
    <li>Keep the setting safe online using appropriate filters, checks and safeguards, monitoring access at all times</li>
    <li>Ensure that children are never placed at risk while in the charge of nursery staff</li>
    <li>Identify changes in staff behaviour and act on these as per the Staff Behaviour Policy</li>
    <li>Take any appropriate action relating to allegations of serious harm or abuse against any person working with children or living or working on the nursery premises including reporting such allegations to Ofsted and other relevant authorities</li>
    <li>Ensure parents are fully aware of child protection policies and procedures when they register with the nursery and are kept informed of all updates when they occur</li>
    <li>Regularly review and update this policy with staff and parents where appropriate and make sure it complies with any legal requirements and any guidance or procedures issued by North Lincolnshire Children’s Services Duty Team.</li>
</ul>

<p>We will support children by offering reassurance, comfort and sensitive interactions. We will devise activities according to individual circumstances to enable children to develop confidence and self-esteem within their peer group and support them to learn how to keep themselves safe.</p>

<h3>Contact information</h3>
<ul class="list-unstyled">
    <li><strong>Local authority children's social care team</strong> - <a href="tel:01724296500" title="Call Local authority children's social care team">01724 296500</a></li>
    <li><strong>Local authority out of hours team</strong> - <a href="tel:01724296555" title="Call Local authority out of hours team">01724 296555</a> or <a href="tel:08081689667" title="Call Local authority out of hours team - freephone">08081 689667</a> (Freephone)</li>
    <li><strong>Police</strong> - <a href="tel:999" title="Call Police Emergency">999</a> (Emergency) or <a href="tel:101" title="Call Police Non Emergency">101</a> (Non Emergency)</li>
    <li><strong>Ofsted</strong> - <a href="tel:03001231231" title="Call Ofsted">0300 123 1231</a></li>
    <li><strong>NSPCC</strong> - <a href="tel:08088005000" title="Call NSPCC">0808 800 5000</a></li>
    <li><strong>Government helpline for extremism concerns</strong> - <a href="tel:02073407264" title="Call government helpline for extremism concerns">020 7340 7264</a> or <a href="mailto:imap@northlincs.gov.uk" title="Email government helpline for extremism concerns">imap@northlincs.gov.uk</a></li>
</ul>

<h3>Types of abuse and particular procedures followed</h3>
<p>Abuse and neglect are forms of maltreatment of a child. Somebody may abuse or neglect a child by harming them or by failing to act to prevent harm. Children may be abused within a family, institution or community setting by those known to them or a stranger. This could be an adult or adults, another child or children. </p>
<h4>What to do if you’re worried a child is being abused (advice for practitioners) 2015.</h4>
<p>The signs and indicators listed below may not necessarily indicate that a child has been abused, but will help us to recognise that something may be wrong, especially if a child shows a number of these symptoms or any of them to a marked degree.</p>

<h2 id="indicators-of-child-abuse">Indicators of child abuse</h2>
<ul>
    <li>Failure to thrive and meet developmental milestones</li>
    <li>Fearful or withdrawn tendencies</li>
    <li>Unexplained injuries to a child or conflicting reports from parents or staff</li>
    <li>Repeated injuries</li>
    <li>Unaddressed illnesses or injuries</li>
    <li>Significant changes to behaviour patterns.</li>
</ul>

<p>Softer signs of abuse as defined by National Institute for Health and Care Excellence (NICE) include:</p>
<ul>
    <li>Low self-esteem</li>
    <li>Wetting and soiling</li>
    <li>Recurrent nightmares</li>
    <li>Aggressive behaviour</li>
    <li>Withdrawing communication</li>
    <li>Habitual body rocking</li>
    <li>Indiscriminate contact or affection seeking</li>
    <li>Over-friendliness towards strangers</li>
    <li>Excessive clinginess</li>
    <li>Persistently seeking attention.</li>
</ul>

<h3>Peer on peer abuse</h3>
<p>We are aware that peer on peer abuse does take place, so we include children in our policies when we talk about potential abusers. This may take the form of bullying, physically hurting another child, emotional abuse, or sexual abuse. We will report this in the same way as we do for adults abusing children, and will take advice from the appropriate bodies on this area.</p>

<h3>Physical abuse</h3>
<p>Action needs to be taken if staff have reason to believe that there has been a physical injury to a child, including deliberate poisoning, where there is definite knowledge or reasonable suspicion that the injury was inflicted or knowingly not prevented. These symptoms may include bruising or injuries in an area that is not usual for a child, e.g. fleshy parts of the arms and legs, back, wrists, ankles and face.</p>

<p>Many children will have cuts and grazes from normal childhood injuries. These should also be logged and discussed with the nursery manager or room leader.</p>

<p>Children and babies may be abused physically through shaking or throwing. Other injuries may include burns or scalds. These are not usual childhood injuries and should always be logged and discussed with the designated safeguarding lead (DSL) and nursery manager.</p>

<h3>Female genital mutilation</h3>
<p>This type of physical abuse is practised as a cultural ritual by certain ethnic groups and there is now more awareness of its prevalence in some communities in England including its effect on the child and any other siblings involved. This procedure may be carried out shortly after birth and during childhood as well as adolescence, just before marriage or during a woman’s first pregnancy and varies widely according to the community . Symptoms may include bleeding, painful areas, acute urinary retention, urinary infection, wound infection, septicaemia, incontinence, vaginal and pelvic infections with depression and post-traumatic stress disorder as well as physiological concerns. If you have concerns about a child relating to this area, you should contact children’s social care team in the same way as other types of physical abuse. There is a mandatory duty to report to police any case where an act of female genital mutilation appears to have been carried out on a girl under the age of 18, we will ensure this is followed in our setting.</p>

<h3>Breast Ironing</h3>
<p>Breast ironing also known as "breast flattening" is the process where young girls' breasts are ironed, massaged and/or pounded down through the use of hard or heated objects in order for the breasts to disappear or delay the development of the breasts entirely. It is believed that by carrying out this act, young girls will be protected from harassment, rape, abduction and early forced marriage. Although this is unlikely to happen to children in the nursery due to their age, we will ensure any signs of this in young adults or older children are followed up using the usual safeguarding referral process.</p>

<h3>Fabricated illness</h3>
<p>This is also a type of physical abuse. This is where a child is presented with an illness that is fabricated by the adult carer. The carer may seek out unnecessary medical treatment or investigation. The signs may include a carer exaggerating a real illness or symptoms, complete fabrication of symptoms or inducing physical illness, e.g. through poisoning, starvation, inappropriate diet. This may also be presented through false allegations of abuse or encouraging the child to appear disabled or ill to obtain unnecessary treatment or specialist support.</p>


<h3>Sexual abuse</h3>
<p>Action needs be taken if the staff member has witnessed an occasion(s) where a child indicated sexual activity through words, play, drawing, had an excessive preoccupation with sexual matters or had an inappropriate knowledge of adult sexual behaviour or language. This may include acting out sexual activity on dolls/toys or in the role play area with their peers, drawing pictures that are inappropriate for a child, talking about sexual activities or using sexual language or words. The child may become worried when their clothes are removed, e.g. for nappy changes.</p>

<p>The physical symptoms may include genital trauma, discharge and bruises between the legs or signs of a sexually transmitted disease (STD). Emotional symptoms could include a distinct change in a child’s behaviour. They may be withdrawn or overly extroverted and outgoing. They may withdraw away from a particular adult and become distressed if they reach out for them, but they may also be particularly clingy to a potential abuser so all symptoms and signs should be looked at together and assessed as a whole.</p>

<p>If a child starts to talk openly to an adult about abuse they may be experiencing the procedure below will be followed:</p>

<h4>Procedure</h4>
<ul>
    <li>The adult should reassure the child and listen without interrupting if the child wishes to talk</li>
    <li>The observed instances will be detailed in a confidential report </li>
    <li>The observed instances will be reported to the nursery manager or DSL</li>
    <li>The matter will be referred to the local authority children’s social care team (see reporting procedures).</li>
</ul>

<h3>Child sexual exploitation (CSE)</h3>
<p>Working Together to Safeguard Children defines CSE as “…a form of child sexual abuse. It occurs where an individual or group takes advantage of an imbalance of power to coerce, manipulate or deceive a child or young person under the age of 18 into sexual activity (a) in exchange for something the victim needs or wants, and/or (b) for the financial advantage or increased status of the perpetrator or facilitator. The victim may have been sexually exploited even if the sexual activity appears consensual. Child sexual exploitation does not always involve physical contact; it can also occur through the use of technology.”</p>

<p>We will be aware of the possibility of CSE and the signs and symptoms this may manifest as. If we have concerns we will follow the same procedures as for other concerns and we will record and refer as appropriate. </p>

<h3>Adult sexual exploitation</h3>
<p>As part of our safeguarding procedures we will also ensure that staff and students are safeguarded from sexual exploitation.</p>

<h3>Emotional abuse</h3>
<p>Action should be taken if the staff member has reason to believe that there is a severe, adverse effect on the behaviour and emotional development of a child, caused by persistent or severe ill treatment or rejection.</p>

<p>This may include extremes of discipline where a child is shouted at or put down on a consistent basis, lack of emotional attachment by a parent, or it may include parents or carers placing inappropriate age or developmental expectations upon them. Emotional abuse may also be imposed through the child witnessing domestic abuse and alcohol and drug misuse by adults caring for them.</p>

<p>The child is likely to show extremes of emotion with this type of abuse. This may include shying away from an adult who is abusing them, becoming withdrawn, aggressive or clingy in order to receive their love and attention. This type of abuse is harder to identify as the child is not likely to show any physical signs.</p>
<h3>Neglect</h3>
<p>Action should be taken if the staff member has reason to believe that there has been any type of neglect of a child (for example, by exposure to any kind of danger, including cold, starvation or failure to seek medical treatment, when required, on behalf of the child), which results in serious impairment of the child's health or development, including failure to thrive.</p>

<p>Signs may include a child persistently arriving at nursery unwashed or unkempt, wearing clothes that are too small (especially shoes that may restrict the child’s growth or hurt them), arriving at nursery in the same nappy they went home in or a child having an illness or identified special educational need or disability that is not being addressed by the parent. A child may also be persistently hungry if a parent is withholding food or not providing enough for a child’s needs.</p>

<p>Neglect may also be shown through emotional signs, e.g. a child may not be receiving the attention they need at home and may crave love and support at nursery. They may be clingy and emotional. In addition, neglect may occur through pregnancy as a result of maternal substance abuse.</p>

<h3>Domestic Abuse / Honour Based Violence / Forced Marriages</h3>
<p>We look at these areas as a child protection concern. Please refer to the separate policy for further details on this.</p>

<h3>Reporting Procedures</h3>
<p>All staff have a responsibility to report safeguarding concerns and suspicions of abuse. These concerns will be discussed with the designated safeguarding lead (DSL) as soon as possible.</p>
<ul>
    <li>Staff will report their concerns to the DSL (in the absence of the DSL they will be reported to the Deputy DSL) </li>
    <li>Any signs of marks/injuries to a child or information a child has given will be recorded and stored securely</li>
    <li>If appropriate, the incident will be discussed with the parent/carer, such discussions will be recorded and the parent will have access to these records on request</li>
</ul>
<p>If there are queries/concerns regarding the injury/information given then the following procedures will take place. The designated safeguarding lead will:</p>

<ul>
    <li>Contact the Local Authority children’s social care team to report concerns and seek advice. If it is believed a child is in immediate danger we will contact the police. If the safeguarding concern relates to an allegation against an adult working or volunteering with children then the DSL will follow the reporting allegations procedure (see below).</li>
    <li>Record the information and action taken relating to the concern raised</li>
    <li>Speak to the parents (unless advised not do so by LA children’s social care team)</li>
    <li>The designated safeguarding lead will follow up with the Local Authority children’s social care team if they have not contacted the setting within the timeframe set out in Working Together to Safeguarding Children (2018). We will never assume that action has been taken.</li>
</ul>


<p>Keeping children safe is our highest priority and if, for whatever reason, staff do not feel able to report concerns to the DSL or deputy DSL they should call the Local Authority children’s social care team or the NSPCC and report their concerns anonymously.</p>

<p>These contact numbers are displayed in the office and staff room.</p>

<h2 id="recording-suspicions-of-abuse-and-disclosures">Recording Suspicions of Abuse and Disclosures</h2>
<p>Staff should make an objective record of any observation or disclosure, supported by the nursery manager or designated safeguarding lead (DSL). This record should include: </p>
<ul>
    <li>Child's name</li>
    <li>Child's address</li>
    <li>Age of the child and date of birth</li>
    <li>Date and time of the observation or the disclosure</li>
    <li>Exact words spoken by the child</li>
    <li>Exact position and type of any injuries or marks seen</li>
    <li>Exact observation of any incident including any concern was reported, with date and time; and the names of any other person present at the time</li>
    <li>Any discussion held with the parent(s) (where deemed appropriate).</li>
</ul>

<p>These records should be signed by the person reporting this and the designated safeguarding officer dated and kept in a separate confidential file. </p>
<p>If a child starts to talk to an adult about potential abuse it is important not to promise the child complete confidentiality. This promise cannot be kept. It is vital that the child is allowed to talk openly and disclosure is not forced or words put into the child’s mouth. As soon as possible after the disclosure details must be logged accurately. </p>

<p>It may be thought necessary that through discussion with all concerned the matter needs to be raised with the local authority children’s social care team and Ofsted. Staff involved may be asked to supply details of any information/concerns they have with regard to a child. The nursery expects all members of staff to co-operate with the local authority children’s social care, police, and Ofsted in any way necessary to ensure the safety of the children.</p>

<p>Staff must not make any comments either publicly or in private about the supposed or actual behaviour of a parent or member of staff.</p>

<h3>Informing parents</h3>
<p>Parents are normally the first point of contact. If a suspicion of abuse is recorded, parents are informed at the same time as the report is made, except where the guidance of the local authority children’s social care team/police does not allow this or will put the child in danger. This will usually be the case where the parent or family member is the likely abuser or where a child may be endangered by this disclosure. In these cases the investigating officers will inform parents.</p>

<h3>Confidentiality</h3>
<p>All suspicions, enquiries and external investigations are kept confidential and shared only with those who need to know. Any information is shared in line with guidance from the local authority.</p>

<h3>Support to families</h3>
<p>The nursery takes every step in its power to build up trusting and supportive relations among families, staff, students and volunteers within the nursery.</p>

<p>The nursery continues to welcome the child and the family whilst enquiries are being made in relation to abuse in the home situation. Parents and families will be treated with respect in a non-judgmental manner whilst any external investigations are carried out in the best interest of the child.</p>

<p>Confidential records kept on a child are shared with the child's parents or those who have parental responsibility for the child, only if appropriate in line with guidance of the local authority with the proviso that the care and safety of the child is paramount. We will do all in our power to support and work with the child's family.</p>

<h3>Allegations against adults working or volunteering with children</h3>
<p>If an allegation is made against a member of staff, student or volunteer or any other person who lives or works on the nursery premises regardless of whether the allegation relates to the nursery premises or elsewhere, we will follow the procedure below.</p>

<p>The allegation should be reported to the senior manager on duty. If this person is the subject of the allegation then this should be reported to the manager/owner <a href="{{ route("staff")."#sharon-holtby" }}">Sharon Holtby</a>, <a href="{{ route("staff")."#kaye-clapson" }}">Kaye Clapson</a> or North Lincolnshire Children’s Duty Team <a href="tel:01724296500" title="Call Single Duty Team">01724 296500</a>.</p>

<p>The Local Authority children’s service duty team and Ofsted will then be informed immediately in order for this to be investigated by the appropriate bodies promptly:</p>

<ul>
    <li>The LADO: Local Authority Children’s service duty team will be informed immediately for advice and guidance by the owner Sharon/Kaye</li>
    <li>If as an individual you feel this will not be taken seriously or are worried about the allegation getting back to the person in question then it is your duty to inform the LADO yourself directly</li>
    <li>A full investigation will be carried out by the appropriate professionals (LADO, Ofsted) to determine how this will be handled </li>
    <li>The nursery will follow all instructions from the LADO and Ofsted and ask all staff members to do the same and co-operate where required</li>
    <li>Support will be provided to all those involved in an allegation throughout the external investigation in line with LADO support and advice</li>
    <li>The nursery reserves the right to suspend any member of staff during an investigation </li>
    <li>All enquiries/external investigations/interviews will be documented and kept in a locked file for access by the relevant authorities</li>
    <li>Unfounded allegations will result in all rights being reinstated</li>
    <li>Founded allegations will be passed on to the relevant organisations including the local authority children’s social care team and where an offence is believed to have been committed, the police. </li>
    <li>Founded allegations will be dealt with as gross misconduct in accordance with our disciplinary procedures and may result in the termination of employment, Ofsted will be notified immediately of this decision. </li>
    <li>The nursery will also notify the Disclosure and Barring Service (DBS) to ensure their records are updated</li>
    <li>All records will be kept until the person reaches normal retirement age or for 21 years and 3 months years if that is longer. This will ensure accurate</li> information is available for references and future DBS checks and avoids any unnecessary reinvestigation </li>
    <li>The nursery retains the right to dismiss any member of staff in connection with founded allegations following an inquiry</li>
    <li>Counselling will be available for any member of the nursery who is affected by an allegation, their colleagues in the nursery and the parents.</li>
</ul>

<h3>Monitoring children’s attendance</h3>
<p>As part of our requirements under the statutory framework and guidance documents we are required to monitor children’s attendance patterns to ensure they are consistent and no cause for concern.</p>

<p>Parents should please inform the nursery prior to their children taking holidays or days off, and all sickness should be called into the nursery on the day so the nursery management are able to account for a child’s absence. </p>

<p>If a child has not arrived at nursery within one hour of their normal start time the parents will be called to ensure the child is safe and healthy. If the parents are not contactable then the further emergency contacts will be used to ensure all parties are safe. </p>

<p>Where a child is part of a child protection plan, or during a referral process, any absences will immediately be reported to the local authority children’s social care team to ensure the child remains safeguarded. </p>

<p>This should not stop parents taking precious time with their children, but enables children’s attendance to be logged so we know the child is safe.</p>

<h3>Looked after children</h3>
<p>As part of our safeguarding practice we will ensure our staff are aware of how to keep looked after children safe. In order to do this we ask that we are informed of:</p>

<ul>
    <li>The legal status of the child (e.g. whether the child is being looked after under voluntary arrangements with consent of parents or on an interim or full care order)</li>
    <li>Contact arrangements for the biological parents (or those with parental responsibility)</li>
    <li>The child’s care arrangements and the levels of authority delegated to the carer by the authority looking after him/her</li>
    <li>The details of the child’s social worker and any other support agencies involved</li>
    <li>Any child protection plan or care plan in place for the child in question.</li>
</ul>

<p>Please refer to the Looked After Children policy for further details. </p>

<h3>Staffing and volunteering</h3>
<p>Our policy is to provide a secure and safe environment for all children. We only allow an adult who is employed by the nursery to care for children and who has an enhanced clearance from the Disclosure and Barring Service (DBS) to be left alone with children. We will obtain enhanced criminal records checks (DBS) for all volunteers and do not allow any volunteers to be unsupervised with children.</p>

<p>All staff will attend child protection training and receive initial basic child protection training during their induction period. This will include the procedures for spotting signs and behaviours of abuse and abusers/potential abusers, recording and reporting concerns and creating a safe and secure environment for the children in the nursery. During induction staff will be given contact details for the local authority children’s social care team and Ofsted to enable them to report any safeguarding concerns, independently, if they feel it necessary to do so.</p>

<p>We have named persons within the nursery who take lead responsibility for safeguarding and co-ordinate child protection and welfare issues, known as the Designated Safeguarding Leads (DSL), there is always at least one designated person on duty during all opening hours of the setting.</p>
<p>These designated persons will receive comprehensive training at least every two years and update their knowledge on an ongoing basis, but at least once a year.</p>

<p>The nursery DSL’s liaise with the local authority children’s social care team, undertakes specific training, including a child protection training course, and receives regular updates to developments within this field. They in turn support the ongoing development and knowledge update of all staff on the team. </p>

<p>Although, under the EYFS, we are only required to have one designated lead for safeguarding, for best practice and to ensure cover at all times, we have two designated leads in place. This enables safeguarding to stay high on our priorities at all times. There will always be at least one designated lead on duty at all times our provision is open. This will ensure that prompt action can be taken if concerns are raised.</p>

<p><strong>The Designated Safeguarding Leads (DSL) at the nursery are</strong>: (North) Clare Grice, Sharon Holtby (South) Kayleigh Carolan, Emma Mansfield (West) Kelly Newton, Sharon Holtby.</p>

<ul>
    <li>We provide adequate and appropriate staffing resources to meet the needs of all children</li>
    <li>Applicants for posts within the nursery are clearly informed that the positions are exempt from the Rehabilitation of Offenders Act 1974. Candidates are informed of the need to carry out checks before posts can be confirmed. Where applications are rejected because of information that has been disclosed, applicants have the right to know and to challenge incorrect information</li>
    <li>We give staff members, volunteers and students regular opportunities to declare changes that may affect their suitability to care for the children. This includes information about their health, medication or about changes in their home life such as child protection plans for their own children </li>
    <li>This information is also stated within every member of staff’s contract </li>
    <li>We request DBS & Health disclosure information on an annual basis</li>
    <li>We abide by the requirements of the EYFS and any Ofsted guidance in respect to obtaining references and suitability checks for staff, students and volunteers, to ensure that all staff, students and volunteers working in the setting are suitable to do so</li>
    <li>We ensure we receive at least two written references BEFORE a new member of staff commences employment with us</li>
    <li>All students will have enhanced DBS checks conducted on them before their placement starts </li>
    <li>Volunteers, including students, do not work unsupervised</li>
    <li>We abide by the requirements of the Safeguarding Vulnerable Groups Act 2006 and the Childcare Act 2006 in respect of any person who is disqualified from providing childcare, is dismissed from our employment, or resigns in circumstances that would otherwise have led to dismissal for reasons of child protection concern</li>
    <li>We have procedures for recording the details of visitors to the nursery and take security steps to ensure that we have control over who comes into the nursery so that no unauthorised person has unsupervised access to the children</li>
    <li>All visitors/contractors will be supervised whilst on the premises, especially when in the areas the children use</li>
    <li>As a staff team we will be fully aware of how to safeguard the whole nursery environment and be aware of potential dangers on the nursery boundaries such as drones or strangers lingering. We will ensure the children remain safe at all times</li>
    <li>The Staff Behaviour Policy sits alongside this policy to enable us to monitor changes in behaviours that may cause concern. All staff sign up to this policy too to ensure any changes are reported to management so we are able to support the individual staff member and ensure the safety and care of the children is not compromised</li>
    <li>All staff have access to and comply with the whistleblowing policy which will enable them to share any concerns that may arise about their colleagues in an appropriate manner</li>
    <li>Signs of inappropriate staff behaviour may include inappropriate sexual comments; excessive one-to-one attention beyond the requirements of their usual role and responsibilities; or inappropriate sharing of images. This is not an exhaustive list, any changes in behaviour must be reported and acted upon immediately</li>
    <li>All staff will receive regular supervision meetings where opportunities will be made available to discuss any issues relating to individual children, child protection training and any needs for further support</li>
    <li>We use peer on peer and manager observations in the setting to ensure that the care we provide for children is at the highest level and any areas for staff development are quickly highlighted. Peer observations allow us to share constructive feedback, develop practice and build trust so that staff are able to share any concerns they may have. Any concerns are raised with the designated lead and dealt with in an appropriate and timely manner</li>
    <p>The deployment of staff within the nursery allows for constant supervision and support. Where children need to spend time away from the rest of the group, the door will be left ajar or other safeguards will be put into action to ensure the safety of the child and the adult.</p>
</ul>

<p>We also operate a Phones and Other Electronic Devices and Social Media policy which states how we will keep children safe from these devices whilst at nursery. This also links to our Online Safety policy.</p>

<h3>Extremism – the Prevent Duty </h3>
<p>Under the Counter-Terrorism and Security Act 2015 we have a duty to refer any concerns of extremism to the police (In Prevent priority areas the local authority will have a Prevent lead who can also provide support). </p>

<p>This may be a cause for concern relating to a change in behaviour of a child or family member, comments causing concern made to a member of the team (or other persons in the setting) or actions that lead staff to be worried about the safety of a child in their care.  We have a Prevent Duty and Radicalisation policy in place. Please refer to this for specific details.</p>

<h3>Online Safety</h3>
<p>We take the safety of our children very seriously and this includes their online safety. Please refer to the Online Safety policy for details on this.</p>

<h3>Human Trafficking and Slavery</h3>
<p>Please refer to our Human Trafficking and Slavery policy for detail on how we keep children safe in this area.</p>

<p>Our nursery has a clear commitment to protecting children and promoting welfare. Should anyone believe that this policy is not being upheld, it is their duty to report the matter to the attention of the nursery manager/owner at the earliest opportunity.</p>

<table class="table">
  <thead>
    <tr>
      <th scope="col">This policy was adopted on</th>
      <th scope="col">Signed on behalf of the nursery</th>
      <th scope="col">Date for review</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">September 2019</th>
      <td></td>
      <td>September 2020</td>
    </tr>
  </tbody>
</table>

@stop
