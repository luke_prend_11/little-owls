@extends('layouts.master')
@section('content')

<p>We believe our staff should be completely attentive during their hours of working to ensure all children in the nursery receive good quality care and education. This is why mobile phones are not to be used during working hours.</p>
<p>We also feel that restrictions need to be placed on staff when they access social networking sites. The nursery has a high reputation to upkeep and comments made on sites such as 'Facebook' could have an impact on how parents using the nursery view the staff.</p>
<p>Staff must adhere to the following:</p>
<ul>
    <li>Mobile phones are not to be turned on during your working hours</li>
    <li>Mobile phones can only be used on a designated break and then this must be away from the children</li>
    <li>Mobile phones should be stored safely in the locked tin provided at all times during the hours of your working day</li>
    <li>During outings, staff will use mobile phones belonging to the nursery wherever possible. No photographs should be taken of the children on any phones, either personal or nursery-owned</li>
    <li>Staff must not post anything onto social networking sites such as 'Facebook' that could be construed to have any impact on the nursery's reputation</li>
    <li>Staff must not post anything onto social networking sites that would offend any other member of staff or parent using the nursery</li>
    <li>If staff choose to allow parents to view their page on social networking sites then this relationship must remain professional at all times</li>
    <li>If any of the above points are not followed then the member of staff involved will face disciplinary action, which could result in dismissal.
</ul>
<h2 id="parents-and-visitors">Parents and visitors</h2>
<ul>
    <li>The nursery operates its own mobile usage policy in relation to staff and visitors to the premises. Whilst we recognise that there may be emergency situations which necessitate the use of a mobile telephone, in order to ensure the safety and welfare of children in our care, parents and visitors are also kindly asked to refrain from using their mobile telephones whilst in the nursery or when collecting or dropping off their children.</li>
</ul>

@stop
