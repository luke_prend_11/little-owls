@extends('layouts.master')
@section('content')

<p>Click on a policy below to read more information.</p>
<ul class="list-unstyled">
    <li><a href="{{ route('policies-child-protection') }}" title="Child Protection Policy">Child Protection Policy</a></li>
    <li><a href="{{ route('policies-confidentiality') }}" title="Little Owls Confidentiality">Little Owls Confidentiality Policy</a></li>
    <li><a href="{{ route('policies-equal-opportunities') }}" title="Equality of Opportunities Policy">Equality of Opportunities Policy</a></li>
    <li><a href="{{ route('policies-smoking') }}" title="Little Owls No Smoking Policy">Little Owls No Smoking Policy</a></li>
    <li><a href="{{ route('policies-mobile-phone') }}" title="Mobile Phone and Social Networking Policy">Mobile Phone and Social Networking Policy</a></li>
    <li><a href="{{ route('policies-complaints') }}" title="Complaints Policy">Complaints Policy</a></li>
    <li><a href="{{ route('policies-admissions') }}" title="Admissions Policy">Admissions Policy</a></li>
    <li><a href="{{ route('policies-behaviour') }}" title="Behaviour Management Policy">Behaviour Management Policy</a></li>
    <li><a href="{{ route('policies-late-collection') }}" title="Late Collection Policy">Late Collection Policy</a></li>
    <li><a href="{{ route('policies-fees-and-funding') }}" title="Fees and Funding Policy">Fees and Funding Policy</a></li>
    <li><a href="{{ route('policies-medication') }}" title="Medication Policy">Medication Policy</a></li>
    <li><a href="{{ route('policies-intimate-care') }}" title="Intimate Care Policy">Intimate Care Policy</a></li>
    <li><a href="{{ route('policies-transition') }}" title="Transition Policy">Transition Policy</a></li>
    <li><a href="{{ route('policies-settling-in') }}" title="Settling in Policy">Settling in Policy</a></li>
    <li><a href="{{ route('policies-healthy-eating') }}" title="Healthy Eating Policy">Healthy Eating Policy</a></li>
</ul>

@stop
