@extends('layouts.master')
@section('content')

<p>All parents agree an approximate arrival time at the nursery and are informed of procedures on what to do if they expect to be late. This includes:</p>
<ul>
    <li>Calling the nursery as soon as possible to advise of their situation</li>
    <li>Asking a designated person to collect their child wherever possible</li>
    <li>Informing the nursery of this person's identity so the nursery can talk to the child if appropriate. This will help to reduce or eliminate the distress that may be caused by this situation</li>
    <li>If the designated person is not known to the nursery staff the parent must provide a detailed description of this person, including their date of birth where known. This designated person must know the individual child's safety password in order for the nursery to release the child into their care. This is the responsibility of the parent.</li>
</ul>
<p>In the instance of a child not being collected from the nursery after a reasonable amount of time [e.g. ½ hour] has been allowed for lateness, the following procedure will be initiated by staff:</p>
<ul>
    <li>Inform the nursery manager that a child has not been collected</li>
    <li>The manager will check for any information regarding changes to normal routines, parents' work patterns or general information. If there is no information recorded, the parents will be contacted on the numbers provided for their mobile, home or work. If this fails the emergency contacts will then be contacted as per the child's records</li>
    <li>The manager/staff member on duty in charge and one other member of staff must stay behind with the child (if it falls outside normal operating hours). During normal operating times, staff ratios must be met and planned for accordingly</li>
    <li>If the parents still have not collected the child, the manager will telephone all contact numbers available every 10 minutes until contact is made. These calls need to be logged on a full incident record</li>
    <li>In the event of no contact being made after one hour has lapsed, the person in charge will ring the Social Services Emergency Duty Team and Ofsted to advise them of the situation</li>
    <li> The two members of staff will remain in the building until suitable arrangements have been made for the collection of the child. The child's welfare and needs will be met at all times</li>
    <li>In order to provide this additional care a late fee of [£50] will be charged to parents. This will pay for any additional operational costs that caring for a child outside their normal nursery hours may incur. </li>
</ul>

@stop
