@extends('layouts.master')
@section('content')


<p>Children's health and well-being is of the utmost importance for the nursery. Smoking has proved to be a health risk and therefore in accordance with legislation, the  nursery operates a strict no smoking policy within its buildings and grounds. It is illegal to smoke in enclosed places.</p>
<p>You are respectfully required to abstain from smoking whilst on the premises. This rule also applies to staff, students, parents, carers, visitors, contractors etc.</p>
<p>Staff accompanying children outside the nursery are not permitted to smoke. We also request that parents accompanying nursery children on outings refrain from smoking whilst caring for the children.</p>
<p>Staff must not smoke whilst wearing nursery uniform as it is essential that staff are positive role models to children and promote a healthy lifestyle.</p>
<p>We respect that it is a personal choice to smoke, although as an organisation we support healthy lifestyles and therefore help staff and parents to stop smoking by:</p>
<ul>
    <li>Providing factsheets and leaflets</li>
    <li>Providing information of local help groups</li>
    <li>Providing details of the NHS quit smoking helpline - <a href="http://www.smokefree.nhs.uk" title="NHS Smoking Helpline" target="_blank">www.smokefree.nhs.uk</a></li>
    <li>Offering information regarding products that are available to help stop smoking</li>
    <li>Offering in-house support.</li>
</ul>

@stop
