@extends('layouts.master')
@section('content')

<p>The nursery’s work with children and their families will bring us into contact with confidential information. It is a legal requirement for the nursery to hold information about the children and families using the nursery and the staff working at the nursery. This information is used for registers, invoices and emergency contacts. However all records will be stored in a locked cabinet in line with Data Protection registration.</p>
<p>It is our intention to respect the privacy of children and their families and we will do so by:</p>
<ul>
    <li>Storing confidential records in a locked filing cabinet</li>
    <li>Ensuring that all staff, volunteers and students are aware that this information is confidential and only for use within the nursery</li>
    <li>Ensuring that parents have access to files and records of their own children but not to those of any other child</li>
    <li>Gaining parental permission for any information to be used other than for the above reasons</li>
    <li>Ensuring the staff, through their close relationship with both the children and their parents, learn more about the families using the nursery.</li>
    <li>Ensuring all staff are aware that this information is confidential and only for use within the nursery setting. If any of this information is requested for whatever reason, the parent’s permission will always be sought</li>
    <li>Ensuring staff do not discuss personal information given by parents with other members of staff, except where it affects planning for the child's needs</li>
    <li>Ensuring staff, student and volunteer inductions include an awareness of the importance of confidentiality in the role of the key person. If staff breach any confidentiality provisions, this may result in disciplinary action, and in serious cases, dismissal. Students on placement in the nursery are advised of our confidentiality policy and required to respect it</li>
    <li>Ensuring staff, students and volunteers are aware of and follow our social networking policy in relation to confidentiality </li>
    <li>Ensuring issues concerning the employment of staff remains confidential to the people directly involved with making personnel decisions</li>
    <li>Ensuring any concerns/evidence relating to a child's personal safety are kept in a secure, confidential file and are shared with as few people as possible on a &quot;need-to-know&quot; basis. If, however, a child is considered at risk, our safeguarding policy will override confidentiality. </li>
</ul>
<p>All the undertakings above are subject to the paramount commitment of the nursery, which is to the safety and  well-being of the child.</p>

@stop
