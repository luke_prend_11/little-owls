@extends('layouts.master')
@section('content')


<p>Our staff are made up of {{ count($allStaff) }} dedicated professionals who most are parents themselves. We are also trained for additional needs. We have all got experiences in different aspects of education but as a team we provide your child with a high standard of quality teaching and learning.</p>
        
        <div class="col-xs-12 col-md-12 no-padding">
        @foreach($allStaff as $staff)
            <div id="{{ str_replace(" ", "-", strtolower($staff->name)) }}" class="staff">
                <span class="col-xs-12 col-sm-3 no-padding name">
                    <h2>{{ $staff->name }}</h2>
                </span>
                <span class="col-xs-12 col-sm-3 no-padding role">{{ $staff->staffPosition->position->name }}{{ $staff->contract == 'Supply' ? " (Supply)" : "" }}</span>        
                <span class="col-xs-12 col-sm-6 no-padding">
                    <ul>
                    @foreach($staff->staffQualifications as $qualification)
                        <li>{{ $qualification->Qualification->name }}</li>
                    @endforeach
                    </ul>
                </span>
            </div>
        @endforeach
        </div>
        
@stop
