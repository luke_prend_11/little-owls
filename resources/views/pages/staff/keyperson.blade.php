@extends('layouts.master')
@section('content')


<h2 id="what-is-a-key-person">What is a key person?</h2>
<p>A key person is a named member of staff assigned to an individual child to support their development and act as the key point of contact with the child's parents or carers. This is their named member of staff with whom a child has more contact than other adults. The key person has special responsibilities for working with a small number of children. The key person system helps build and develop positive relationships with children and between parents, carers and staff.</p>

<h2 id="meeting-the-requirements-of-the-early-years-foundation-stage-eyfs">Meeting the requirements of the Early Years Foundation Stage (EYFS)</h2>
<ul>
    <li>Each child will be assigned a key person who will help them to become familiar with their surroundings, to feel confident and safe within it, and develop a genuine bond with the child and immediate family that forms the basis of a settled, close relationship. If a child does not bond with their initial key person this will be changed to the practitioner they develop a relationship with the best.</li>
    <li>The key person will meet the needs of each child and respond sensitively to their feelings, behaviour and ideas.</li>
    <li>A child's patterns of attendance will be considered when appointing a key person.</li>
    <li>The nursery will identify a key person by name and photographs on display in each room.</li>
</ul>
<p>The key person will;</p>
<ul>
    <li>Actively build positive relationships with clear lines of communication between the children and their families</li>
    <li>Observe and plan for children's likes, interests and individual needs</li>
    <li>Ensure that children's physical needs are met sensitively</li>
    <li>Develop a secure and trusting relationship by learning key words in a child's first language, or acknowledge their sounds and gestures</li>
    <li>Share the child's 'learning journey' regularly with parents, and value their written or verbal contributions</li>
    <li>Support a child through transitional periods when changing settings or starting school, and during key milestone periods</li>
    <li>Develop trust to enable children's independence</li>
    <li>Plan for all shared communications and transfer of documents when transition to a new key person or setting/school is due</li>
</ul>
<p>The nursery will;</p>
<ul>
    <li>Ensure that the child's key person is available during new situations, or at times of anxiety or illness</li>
    <li>Provide regular support for key persons with their supervisor, or during staff meetings to ensure that there is time to reflect on issues or concerns of children and their families</li>
    <li>Hold 3 parents evenings a year using an appointments system to ensure that every family has time for discussions with their assigned key person</li>
</ul>

@stop
