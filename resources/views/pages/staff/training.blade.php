@extends('layouts.master')
@section('content')

<p>{{ config('constants.SHORT_SITE_NAME') }} management encourage and support staff in their professional development. We divide Staff training and development in to two categories:</p>

<h2 id="compulsory-short-courses">Compulsory Short Courses</h2>
<p><strong>These will be paid for</strong> and include:</p>
<ol>
    <li>Safeguarding</li>
    <li>Risk assessment</li>
    <li>First Aid</li>
    <li>Food Hygiene</li>
    <li>Behaviour management</li>
    <li>For SENCO – all relevant SENCO courses</li>
    <li>For Manager – health and safety management</li>
</ol>

<h2 id="noncompulsory-courses">Non-compulsory Courses</h2>
<ol>
    <li>Short courses not included above – at the discretion of the managers for all staff. e.g ECaT</li>
    <li>Longer term courses such as NVQ level 2, 3 in FDA, BA in a related  area of knowledge in early years and at the choosing of the member of staff. Staff should try to obtain grant funding for fees, as Little Owls has a limited budget available. Little Owls management will always try to support and encourage staff who wish to go on to further study academically. Financial support such as time off for study or exams is at the discretion of the managers. All staff will be supported to gain a relevant level 3 qualification</li>
</ol>

@stop
