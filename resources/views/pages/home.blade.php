@extends('layouts.master')
@section('intro-content')

                <h2>2 to 5 Years</h2>
                <p>We are delighted to manage a wonderful team here at {{ config('constants.SHORT_SITE_NAME') }} who offer an outstanding service to suit all of your childcare needs and very much understand that when it comes to choosing childcare, it can be a very stressful event for any parent.</p>
                <p>{{ config('constants.SHORT_SITE_NAME') }} is <strong>committed to ensuring that your child’s individual needs are fully catered for</strong> and believe that this is one of the most important and rapid stages in your child’s development. We aim to deliver <strong>high quality learning and care</strong> that enhances the development of the children within our setting and gives these children the best possible start in life.</p>
                <p>We will work in partnership with you to <strong>ensure your child’s development is encouraged in a safe, happy and secure environment</strong> and our staff are fully trained to provide the care, education and development opportunities that are so crucial to each individual child’s needs.</p>
                <p>We very much hope you will join us and look forward to meeting you and your family.</p>
                <p><a href="{{ route("staff")."#sharon-holtby" }}">Sharon Holtby</a><br />
                Nursery Owner</p>
@stop

@section('ofsted-content')

                <div id="ofsted">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <h3>Ofsted</h3>
                                <p>The latest Ofsted Report for {{ config('constants.SHORT_SITE_NAME') }} was in October 2013 and gave us an overall<br />
                                    judgement of <strong>Outstanding</strong>! You can read the full report or find out more information by<br />
                                    clicking the relevant link below.</p>
                                <a href="{{ URL::asset("downloads/Ofsted_2013.pdf") }}" class="btn btn-primary btn-block">Read Full Report</a>
                                <a href="{{ route("ofsted") }}" class="btn btn-secondary btn-block">Past Ofsted Inspections</a>
                                <a href="https://www.gov.uk/government/organisations/ofsted" class="btn btn-tertiary btn-block" target="_blank">More Information</a>
                            </div>
                        </div>
                    </div>
                </div>
@stop

@section('content')

                <div class="col-xs-12 col-md-4 news">
                    <h3><i class="fas fa-rss"></i>News &amp; Bulletins</h3>
                    <div>
                        @if(isset($homePageNews))
                            @foreach($homePageNews as $newsItem)
                            <div class="news-item">
                                <h4>{{ $newsItem->title }}</h4>
                                <p>
                                    {!! $newsItem->contents !!}
                                    @if($newsItem->download)
                                        <a href="{{ URL::asset("downloads/".$newsItem->download->file_name) }}" title="{{ $newsItem->link_text }}">
                                            {{ $newsItem->link_text }}
                                        </a>
                                    @endif
                                    .
                                </p>
                            </div>
                            @endforeach
                        @endif
                    </div>
                    <a href="{{ route('newsletters') }}" class="btn btn-primary btn-block">All News<i class="fas fa-rss"></i></a>
                </div>
                <div class="col-xs-12 col-md-4 health">
                    <h3><i class="far fa-heart"></i>Healthy Eating</h3>
                    <p>It’s important for children to have a nutritious diet in order to <strong>maximise their learning potential</strong>.</p>
                    <p>Why not try out some <strong>healthy packed lunch alternatives</strong>. Our healthy eating poster might give you some inspiration on packed lunches!</p>
                    <p>Read our <a href="{{ route('policies-healthy-eating') }}" title="Healthy Eating Policy">Healthy Eating Policy</a> here.</p>
                    <a href="{{ URL::asset("downloads/Packed_Lunch_Ideas.pdf") }}" class="btn btn-secondary btn-block">Healthy Eating Poster<i class="far fa-heart"></i></a>
                </div>
                <div class="col-xs-12 col-md-4 terms">
                    <h3><i class="far fa-calendar-alt"></i>Term Dates</h3>
                    <div>
                        @if(isset($nextHoliday) && count($nextHoliday) > 0)
                            @include('includes.home-term-dates', ['holidays' => $nextHoliday])
                        @endif
                        @if(isset($nextNextHoliday) && count($nextNextHoliday) > 0)
                            @include('includes.home-term-dates', ['holidays' => $nextNextHoliday])
                        @endif
                    </div>
                    <a href="{{ route('term-dates') }}" class="btn btn-tertiary btn-block">All Term Dates<i class="far fa-calendar-alt"></i></a>
                </div>
@stop
