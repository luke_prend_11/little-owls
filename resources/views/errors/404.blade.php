@extends('layouts.master')
@section('content')

            <p>The page you are looking for is unavailable. If you entered a web address please check it is correct.</p>
            <p>We have recently updated our website so you may have been directed to a page that is no longer available. If you would like more information on something then please contact us and we will be happy to help.</p>
            
            <a class="btn btn-primary btn-block" href="{{ route('home') }}" title="Go to Homepage">Visit <strong>Homepage</strong></a>
            <a class="btn btn-secondary btn-block" href="{{ route('contact') }}" title="Contact Us"><strong>Contact</strong> Us</a>
            
@stop