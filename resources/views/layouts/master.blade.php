{{ Meta::setMetaData() }} 
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    @include('includes.head')
    
</head>
<body>
    <header>
        @include('includes.header')
        
    </header>

    @if (Route::currentRouteName() == 'home')    
    <div id="intro">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h1>{{ Meta::$pageH1 }}</h1>
                    @yield('intro-content')
                </div>
            </div>
        </div>
    </div>
    
    @yield('ofsted-content')
    @endif
    
    <div id="main-content">
        <div class="container">
            <!-- show breadcrumb if not home page -->
            @if (Route::currentRouteName() != 'home' && Meta::$currentPageRoute != "404")
            <div class="row">
                <div class="col-xs-12 col-md-12">
                {!! Breadcrumbs::render() !!}
                </div>
            </div>
            @endif
            <div class="row">
                @if (Route::currentRouteName() != 'home')
                    @if (Meta::$headingsLinks)
                        <div class="col-xs-12 col-md-10">
                    @else
                        <div class="col-xs-12 col-md-12">
                    @endif
                            <h1>{{ Meta::$pageH1 }}</h1>
                            @yield('content')
                        </div>
                    @if (Meta::$headingsLinks)
                        <div class="col-xs-12 col-md-2 pull-right">
                            @include('includes.page-headings')
                        </div>
                    @endif
                @else
                    @yield('content')
                    
                @endif
            </div>
        </div>
    </div>
    
    <footer>
        <div class="container">
            <div class="row">
                @include('includes.footer')
            
            </div>
        </div>
    </footer>
    <!-- get list of h2s for sidebar -->
    <script type="text/javascript">
        var headings = $('<ul />');
        $('h2').each(function() {
            anchor = $(this).text().toLowerCase().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
            headings.append('<li><a href="#' + anchor + '">' + $(this).text() + '</a></li>');
        });
        $('#headings').append(headings);
    </script>
    
    <!-- smooth scroll to anchor links in page -->
    <script type="text/javascript" src="{{ URL::asset('js/smooth-scroll.js') }}"></script>
    
    <script>
    window.onscroll = function() {myFunction();};

    var navbar = document.getElementById("navbar");
    var header = document.getElementsByTagName("header")[0];
    var sticky = navbar.offsetTop;

    function myFunction() {
      if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky");
        header.classList.add("sticky");
      } else {
        navbar.classList.remove("sticky");
        header.classList.remove("sticky");
      }
    }
    </script>
</body>
</html>