@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <ul>
                        <li><a href="{{ route('admin-news') }}">News</a></li>
                        <li>School Holidays</li>
                        <li>Staff</li>
                        <li>Staff Qualifications</li>
                        <li>Staff Roles</li>
                    </ul>
                    
                    @foreach ($news as $newsItem)
                        {{ $newsItem->title }}
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
