<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_holidays', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id');
            $table->integer('holiday_id');
            $table->integer('year_id');
            $table->date('closes');
            $table->date('re_opens');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_holidays');
    }
}
