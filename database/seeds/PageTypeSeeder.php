<?php

use Illuminate\Database\Seeder;

class PageTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page_types')->insert([
            [
                'id' => 1,
                'type' => 'curriculum',
            ],
            [
                'id' => 2,
                'type' => 'policy',
            ],
            [
                'id' => 3,
                'type' => 'staff',
            ],
        ]);
    }
}
