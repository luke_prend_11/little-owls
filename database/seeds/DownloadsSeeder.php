<?php

use Illuminate\Database\Seeder;

class DownloadsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('downloads')->insert([
            [
                'id' => 1,
                'file_name' => '2_Year_Old_Funding.docx',
                'created_at' => '2013-09-05',
                'updated_at' => '2013-09-05',
            ],
            [
                'id' => 2,
                'file_name' => 'Newsletter_April_2014.docx',
                'created_at' => '2014-04-28',
                'updated_at' => '2014-04-28',
            ],
            [
                'id' => 3,
                'file_name' => 'Newsletter_February_2013.pdf',
                'created_at' => '2013-02-28',
                'updated_at' => '2013-02-28',
            ],
            [
                'id' => 4,
                'file_name' => 'Newsletter_February_2017.pdf',
                'created_at' => '2017-02-28',
                'updated_at' => '2017-02-28',
            ],
            [
                'id' => 5,
                'file_name' => 'Newsletter_January_2014.pdf',
                'created_at' => '2014-01-28',
                'updated_at' => '2014-01-28',
            ],
            [
                'id' => 6,
                'file_name' => 'Newsletter_January_2015.pdf',
                'created_at' => '2015-01-28',
                'updated_at' => '2015-01-28',
            ],
            [
                'id' => 7,
                'file_name' => 'Newsletter_March_2016.pdf',
                'created_at' => '2016-03-28',
                'updated_at' => '2016-03-28',
            ],
            [
                'id' => 8,
                'file_name' => 'Newsletter_May_2013.pdf',
                'created_at' => '2013-05-28',
                'updated_at' => '2013-05-28',
            ],
            [
                'id' => 9,
                'file_name' => 'Newsletter_September_2012.pdf',
                'created_at' => '2012-09-28',
                'updated_at' => '2012-09-28',
            ],
            [
                'id' => 10,
                'file_name' => 'Newsletter_September_2013.pdf',
                'created_at' => '2013-09-28',
                'updated_at' => '2013-09-28',
            ],
            [
                'id' => 11,
                'file_name' => 'Newsletter_September_2014.pdf',
                'created_at' => '2014-09-28',
                'updated_at' => '2014-09-28',
            ],
            [
                'id' => 12,
                'file_name' => 'Ofsted_2011.pdf',
                'created_at' => '2011-09-27',
                'updated_at' => '2011-09-27',
            ],
            [
                'id' => 13,
                'file_name' => 'Ofsted_2013.pdf',
                'created_at' => '2013-12-15',
                'updated_at' => '2013-12-15',
            ],
            [
                'id' => 14,
                'file_name' => 'Packed_lunch_ideas.pdf',
                'created_at' => '2012-05-09',
                'updated_at' => '2012-05-09',
            ],
        ]);
    }
}
