<?php

use Illuminate\Database\Seeder;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('staff')->insert([
            [
                'name' => 'Clare Grice',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Courtney Barber',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Emma Mansfield',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Erika Wilkins',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Karen Cooper',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Karen Longbottom',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Kayleigh Carolan',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Kelly Newton',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Lorraine Kirman',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Rebecca Holtby',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Rosanna Ali',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Sharon Holtby',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Tracy Bolland',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Valerie McMillan',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Vicky Allen',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Caroline Hicks',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Emma Blanchard',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Kerry Leake',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Ashleigh Hunter',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Donna Macneil',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Joanne Whitely',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Abigail Wright',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Lisa Cleary',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Natasha McKeown',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Katie Lappin',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Caitlin Waude',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Kayley Grice',
                'contract' => 'Permanent',
            ],
            [
                'name' => 'Kirsty Squire',
                'contract' => 'Permanent',
            ]
        ]);
    }
}
