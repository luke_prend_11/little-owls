<?php

use Illuminate\Database\Seeder;

class CurriculumsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('curriculums')->insert([
            [
                'name' => 'Personal Social &amp; Emotional Development',
                'short-name' => 'PSED',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Communication &amp; Language Development',
                'short-name' => 'Communication',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Physical Development',
                'short-name' => 'PD',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Literacy',
                'short-name' => 'Literacy',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Mathematics',
                'short-name' => 'Maths',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Understanding The World',
                'short-name' => 'The World',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Expressive Arts &amp; Design',
                'short-name' => 'Expressive Arts',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
