<?php

use Illuminate\Database\Seeder;
use App\Http\Controllers\StaffController;

class StaffPositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staffController = new StaffController();

        DB::table('staff_positions')->insert([
            [
                'staff_id' => $staffController->getStaffId('Clare Grice'),
                'position_id' => $staffController->getStaffPositionId('Manager'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Courtney Barber'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Emma Mansfield'),
                'position_id' => $staffController->getStaffPositionId('EYTS'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Erika Wilkins'),
                'position_id' => $staffController->getStaffPositionId('Senco'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Cooper'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Longbottom'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kayleigh Carolan'),
                'position_id' => $staffController->getStaffPositionId('Manager'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kelly Newton'),
                'position_id' => $staffController->getStaffPositionId('Manager'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Lorraine Kirman'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rebecca Holtby'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rosanna Ali'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Sharon Holtby'),
                'position_id' => $staffController->getStaffPositionId('Nursery Owner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Tracy Bolland'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Valerie McMillan'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Vicky Allen'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Caroline Hicks'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Emma Blanchard'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kerry Leake'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Ashleigh Hunter'),
                'position_id' => $staffController->getStaffPositionId('Communications Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Donna Macneil'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Joanne Whitely'),
                'position_id' => $staffController->getStaffPositionId('Senco'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Abigail Wright'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Lisa Cleary'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Natasha McKeown'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Katie Lappin'),
                'position_id' => $staffController->getStaffPositionId('Senco'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Caitlin Waude'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kayley Grice'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kirsty Squire'),
                'position_id' => $staffController->getStaffPositionId('Child Practitioner'),
            ]
        ]);
    }
}
