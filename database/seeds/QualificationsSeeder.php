<?php

use Illuminate\Database\Seeder;

class QualificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('qualifications')->insert([
            [
                'name' => 'BA Honours degree in Social Science',
            ],
            [
                'name' => 'BA Honours (First Class) in Early Childhood Studies',
            ],
            [
                'name' => 'BA Honours in Psychology',
            ],
            [
                'name' => 'Behaviour Officer',
            ],
            [
                'name' => 'Designated Person For Looked After Children',
            ],
            [
                'name' => 'Early Years Teacher Status (EYTS)',
            ],
            [
                'name' => 'Equality Of Opportunity Officer',
            ],
            [
                'name' => 'Food Safety Level 2',
            ],
            [
                'name' => 'Foundation Degree in Early Years',
            ],
            [
                'name' => 'Health &amp; Safety Officer',
            ],
            [
                'name' => 'Master\'s Degree in Psychology',
            ],
            [
                'name' => 'Nneb Diploma',
            ],
            [
                'name' => 'NVQ Level 2 in Childcare &amp; Education',
            ],
            [
                'name' => 'NVQ Level 3 in Childcare &amp; Education',
            ],
            [
                'name' => 'NVQ Level 5 in Childcare &amp; Education',
            ],
            [
                'name' => 'Outside Agencies Liaison Officer',
            ],
            [
                'name' => 'Paediatric First Aid',
            ],
            [
                'name' => 'Qualified Tutor (7306)',
            ],
            [
                'name' => 'Safeguarding Awareness',
            ],
            [
                'name' => 'Safeguarding Officer (1)',
            ],
            [
                'name' => 'Safeguarding Officer (2)',
            ],
            [
                'name' => 'Safeguarding Officer (3)',
            ],
            [
                'name' => 'SENCO',
            ],
            [
                'name' => 'Working towards Foundation Degree in Early Years',
            ],
            [
                'name' => 'Working towards BA Honours in Early Childhood Studies',
            ],
            [
                'name' => 'Safeguarding Foundation',
            ],
            [
                'name' => 'Level 3 Diploma Early Years Workforce',
            ],
            [
                'name' => 'Food Hygiene',
            ],
            [
                'name' => 'Level 2 Children &amp; Young People Workforce',
            ],
            [
                'name' => 'Level 3 Children &amp; Young People Workforce',
            ],
            [
                'name' => 'Cache Level 3 Diploma in Early Years',
            ],
            [
                'name' => 'Early Childhood Studies BA',
            ],
            [
                'name' => 'Safeguarding Level 2',
            ],
            [
                'name' => 'BA Honours in Early Childhood Studies',
            ],
            [
                'name' => 'Working towards Early Years Teacher (EYT)',
            ],
            [
                'name' => 'Level 2 Teaching Assistant',
            ],
            [
                'name' => 'Working towards Level 3 in Childcare &amp; Education',
            ],
            [
                'name' => 'Working towards Level 5 in Childcare &amp; Education',
            ],
            [
                'name' => 'Teaching Assistant Level 3',
            ],
        ]);
    }
}
