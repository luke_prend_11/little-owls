<?php

use Illuminate\Database\Seeder;
use App\Staff;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allStaff = Staff::all();
        
        foreach($allStaff as $staff)
        {
            $newArray[] = array(
                'staff_id' => $staff->id,
                'email' => $this->getEmailAddress($staff->name),
                'password' => $this->makePassword($staff->name),
            );
        }
        
        DB::table('users')->insert($newArray);
    }
        
    private function getEmailAddress($name)
    {
        return $this->convertNameForEmailAndPassword($name).config('constants.CONTACT_EMAIL_DOMAIN');
    }
    
    private function makePassword($name)
    {
        return Hash::make($this->convertNameForEmailAndPassword($name));
    }
    
    private function convertNameForEmailAndPassword($name)
    {
        return str_replace(" ", ".", strtolower($name));
    }
}
