<?php

use Illuminate\Database\Seeder;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schools')->insert([
            [
                'name' => 'Avenue Vivian',
                'location' => 'North',
            ],
            [
                'name' => 'Dragonby Road',
                'location' => 'South',
            ],
            [
                'name' => 'Lichfield Avenue',
                'location' => 'West',
            ],
        ]);
    }
}
