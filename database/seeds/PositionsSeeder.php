<?php

use Illuminate\Database\Seeder;

class PositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            [
                'name' => 'Child Practitioner',
            ],
            [
                'name' => 'Communications Practitioner',
            ],
            [
                'name' => 'Deputy Manager',
            ],
            [
                'name' => 'Deputy Manager/Administrator',
            ],
            [
                'name' => 'EYTS',
            ],
            [
                'name' => 'Manager',
            ],
            [
                'name' => 'Nursery Owner',
            ],
            [
                'name' => 'Office Administrator',
            ],
            [
                'name' => 'Office Administrator/Practitioner',
            ],
            [
                'name' => 'Senco',
            ],
            [
                'name' => 'Senior Child Practitioner',
            ],
            [
                'name' => 'Senior Office Administrator',
            ],
            [
                'name' => 'Room Leader',
            ],
            [
                'name' => 'Deputy Manager/Senco',
            ],
            [
                'name' => 'Supervisor',
            ],
        ]);
    }
}
