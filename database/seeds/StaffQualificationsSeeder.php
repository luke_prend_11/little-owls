<?php

use Illuminate\Database\Seeder;
use App\Http\Controllers\StaffController;

class StaffQualificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staffController = new StaffController();

        DB::table('staff_qualifications')->insert([
            [
                'staff_id' => $staffController->getStaffId('Clare Grice'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Clare Grice'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Clare Grice'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Clare Grice'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Courtney Barber'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Courtney Barber'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Courtney Barber'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Courtney Barber'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Emma Mansfield'),
                'qualification_id' => $staffController->getQualificationId('Early Years Teacher Status (EYTS)'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Emma Mansfield'),
                'qualification_id' => $staffController->getQualificationId('Master\'s Degree in Psychology'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Emma Mansfield'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Emma Mansfield'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Erika Wilkins'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Erika Wilkins'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Erika Wilkins'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Erika Wilkins'),
                'qualification_id' => $staffController->getQualificationId('Foundation Degree in Early Years'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Erika Wilkins'),
                'qualification_id' => $staffController->getQualificationId('Behaviour Officer'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Cooper'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Cooper'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Cooper'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Cooper'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Cooper'),
                'qualification_id' => $staffController->getQualificationId('Health &amp; Safety Officer'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Longbottom'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Longbottom'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Longbottom'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Karen Longbottom'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kayleigh Carolan'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kayleigh Carolan'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kayleigh Carolan'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kayleigh Carolan'),
                'qualification_id' => $staffController->getQualificationId('Behaviour Officer'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kayleigh Carolan'),
                'qualification_id' => $staffController->getQualificationId('Working towards BA Honours in Early Childhood Studies'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kelly Newton'),
                'qualification_id' => $staffController->getQualificationId('BA Honours degree in Social Science'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kelly Newton'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kelly Newton'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kelly Newton'),
                'qualification_id' => $staffController->getQualificationId('Working towards Early Years Teacher (EYT)'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Lorraine Kirman'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Lorraine Kirman'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Lorraine Kirman'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Lorraine Kirman'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Lorraine Kirman'),
                'qualification_id' => $staffController->getQualificationId('Health &amp; Safety Officer'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rebecca Holtby'),
                'qualification_id' => $staffController->getQualificationId('BA Honours in Psychology'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rebecca Holtby'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rebecca Holtby'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rebecca Holtby'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rebecca Holtby'),
                'qualification_id' => $staffController->getQualificationId('Behaviour Officer'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rosanna Ali'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 2 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rosanna Ali'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rosanna Ali'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Rosanna Ali'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Sharon Holtby'),
                'qualification_id' => $staffController->getQualificationId('Foundation Degree in Early Years'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Sharon Holtby'),
                'qualification_id' => $staffController->getQualificationId('Qualified Tutor (7306)'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Sharon Holtby'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Foundation'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Sharon Holtby'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Officer (2)'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Sharon Holtby'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Tracy Bolland'),
                'qualification_id' => $staffController->getQualificationId('Nneb Diploma'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Tracy Bolland'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Tracy Bolland'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Tracy Bolland'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Valerie McMillan'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Valerie McMillan'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Valerie McMillan'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Vicky Allen'),
                'qualification_id' => $staffController->getQualificationId('Foundation Degree in Early Years'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Vicky Allen'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Vicky Allen'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Caroline Hicks'),
                'qualification_id' => $staffController->getQualificationId('Level 3 Diploma Early Years Workforce'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Caroline Hicks'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Caroline Hicks'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Caroline Hicks'),
                'qualification_id' => $staffController->getQualificationId('Food Hygiene'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Caroline Hicks'),
                'qualification_id' => $staffController->getQualificationId('Working towards Level 5 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kerry Leake'),
                'qualification_id' => $staffController->getQualificationId('Level 3 Children &amp; Young People Workforce'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kerry Leake'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kerry Leake'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kerry Leake'),
                'qualification_id' => $staffController->getQualificationId('Food Hygiene'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Emma Blanchard'),
                'qualification_id' => $staffController->getQualificationId('Cache Level 3 Diploma in Early Years'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Emma Blanchard'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Emma Blanchard'),
                'qualification_id' => $staffController->getQualificationId('Food Hygiene'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Emma Blanchard'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Ashleigh Hunter'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Ashleigh Hunter'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Ashleigh Hunter'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Ashleigh Hunter'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Donna Macneil'),
                'qualification_id' => $staffController->getQualificationId('Teaching Assistant Level 3'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Donna Macneil'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Donna Macneil'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Donna Macneil'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Joanne Whitely'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Joanne Whitely'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Joanne Whitely'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Joanne Whitely'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Abigail Wright'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Abigail Wright'),
                'qualification_id' => $staffController->getQualificationId('Safeguarding Awareness'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Abigail Wright'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Abigail Wright'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Lisa Cleary'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 5 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Lisa Cleary'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Lisa Cleary'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Natasha McKeown'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Natasha McKeown'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Natasha McKeown'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Katie Lappin'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Katie Lappin'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Katie Lappin'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Caitlin Waude'),
                'qualification_id' => $staffController->getQualificationId('Working towards Level 5 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Caitlin Waude'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Caitlin Waude'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kayley Grice'),
                'qualification_id' => $staffController->getQualificationId('Working towards Early Years Teacher (EYT)'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kayley Grice'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kayley Grice'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kirsty Squire'),
                'qualification_id' => $staffController->getQualificationId('NVQ Level 3 in Childcare &amp; Education'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kirsty Squire'),
                'qualification_id' => $staffController->getQualificationId('Paediatric First Aid'),
            ],
            [
                'staff_id' => $staffController->getStaffId('Kirsty Squire'),
                'qualification_id' => $staffController->getQualificationId('Food Safety Level 2'),
            ],
        ]);
    }
}
