<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->truncate();
        DB::table('news')->insert([
            [
                'title' => 'Little Owls Now has 3 Settings',
                'contents' => 'Avenue Vivian, Scunthorpe, DN15 8LG (North). Dragonby Road, Scunthorpe, DN17 2LD (South). Lichfield Avenue, Scunthorpe, DN17 1QL (West)',
                'download_id' => NULL,
                'link_text' => '',
                'button_text' => '',
                'created_at' => '2017-11-01',
                'updated_at' => '2017-11-01',
            ],
            [
                'title' => '2 Year Old Funding Information',
                'contents' => 'Your 2 year old could now be entitled to 15 hours free childcare each week.',
                'download_id' => 1,
                'link_text' => 'For more information download our information sheet',
                'button_text' => 'Download Information Sheet',
                'created_at' => '2017-08-23',
                'updated_at' => '2017-08-23',
            ],
            [
                'title' => 'Ofsted 2013',
                'contents' => 'We got Outstanding! Download the',
                'download_id' => 13,
                'link_text' => 'full report',
                'button_text' => 'Download Report',
                'created_at' => '2013-12-15',
                'updated_at' => '2013-12-15',
            ],
            [
                'title' => 'Ofsted 2011',
                'contents' => 'We got Outstanding! Download the',
                'download_id' => 12,
                'link_text' => 'Full Report',
                'button_text' => 'Download Report',
                'created_at' => '2011-09-27',
                'updated_at' => '2011-09-27',
            ],
            [
                'title' => 'Healthy Packed Lunch Ideas',
                'contents' => 'Get ideas for packed lunches from our',
                'download_id' => 14,
                'link_text' => 'healthy lunches poster',
                'button_text' => 'Healthy Packed Lunch Ideas',
                'created_at' => '2012-05-09',
                'updated_at' => '2012-05-09',
            ],
            [
                'title' => 'Christmas Closing Dates',
                'contents' => 'North setting will be closing for christmas on Friday 13 December. South and West settings will close on Thursday 19th December. <a href="about/term-dates" title="All term dates">View all term dates</a>',
                'download_id' => NULL,
                'link_text' => '',
                'button_text' => '',
                'created_at' => '2019-11-12',
                'updated_at' => '2019-11-12',
            ],
        ]);
    }
}
