<?php

use Illuminate\Database\Seeder;

class SchoolYears extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('school_years')->truncate();

        DB::table('school_years')->insert([
            [
                'name' => '2016/17',
                'opens' => '2016-09-05',
                'closes' => '2017-07-21',
            ],
            [
                'name' => '2017/18',
                'opens' => '2017-09-04',
                'closes' => '2018-07-20',
            ],
            [
                'name' => '2018/19',
                'opens' => '2018-09-04',
                'closes' => '2019-07-19',
            ],
            [
                'name' => '2019/20',
                'opens' => '2019-09-03',
                'closes' => '2020-07-17',
            ],
            [
                'name' => '2020/21',
                'opens' => '2020-09-07',
                'closes' => '2021-07-22',
            ],
            [
                'name' => '2021/22',
                'opens' => '2021-09-06',
                'closes' => '2022-07-22',
            ],
            [
                'name' => '2022/23',
                'opens' => '2022-09-07',
                'closes' => '2023-07-22',
            ],
            [
                'name' => '2023/24',
                'opens' => '2023-09-07',
                'closes' => '2024-07-22',
            ],
            [
                'name' => '2024/25',
                'opens' => '2024-09-07',
                'closes' => '2025-07-22',
            ],
            [
                'name' => '2025/26',
                'opens' => '2025-09-07',
                'closes' => '2026-07-22',
            ],
        ]);
    }
}
