<?php

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'page_title' => 'Personal Social &amp; Emotional Development',
                'short_name' => 'PSED',
                'page_type' => 1,
                'meta_description' => '',
                'css' => '',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Communication &amp; Language Development',
                'short_name' => 'Communication',
                'page_type' => 1,
                'meta_description' => '',
                'css' => '',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Physical Development',
                'short_name' => 'PD',
                'page_type' => 1,
                'meta_description' => '',
                'css' => '',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Literacy',
                'short_name' => 'Literacy',
                'page_type' => 1,
                'meta_description' => '',
                'css' => '',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Mathematics',
                'short_name' => 'Maths',
                'page_type' => 1,
                'meta_description' => '',
                'css' => '',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Understanding The World',
                'short_name' => 'The World',
                'page_type' => 1,
                'meta_description' => '',
                'css' => '',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Expressive Arts &amp; Design',
                'short_name' => 'Expressive Arts',
                'page_type' => 1,
                'meta_description' => '',
                'css' => '',
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
