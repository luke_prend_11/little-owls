<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(CurriculumsSeeder::class);
        $this->call(PageTypeSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(DownloadsSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(QualificationsSeeder::class);
        $this->call(StaffSeeder::class);
        $this->call(StaffQualificationsSeeder::class);
        $this->call(PositionsSeeder::class);
        $this->call(StaffPositionsSeeder::class);
        $this->call(SchoolYears::class);
        $this->call(Holidays::class);
        $this->call(SchoolSeeder::class);
        $this->call(SchoolHolidays::class);
        $this->call(UsersSeeder::class);
    }
}
