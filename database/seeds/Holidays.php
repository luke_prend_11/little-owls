<?php

use Illuminate\Database\Seeder;

class Holidays extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('holidays')->truncate();

        DB::table('holidays')->insert([
            [
                'name' => 'Autumn Half Term',
                'display' => 1,
                'order' => 2
            ],
            [
                'name' => 'Christmas',
                'display' => 1,
                'order' => 3
            ],
            [
                'name' => 'Spring Half Term',
                'display' => 1,
                'order' => 4
            ],
            [
                'name' => 'Easter',
                'display' => 1,
                'order' => 5
            ],
            [
                'name' => 'Summer Half Term',
                'display' => 1,
                'order' => 6
            ],
            [
                'name' => 'Summer',
                'display' => 1,
                'order' => 7
            ],
            [
                'name' => 'Boxing Day',
                'display' => 0,
                'order' => 0
            ],
            [
                'name' => 'Boxing Day (substitute day)',
                'display' => 0,
                'order' => 0
            ],
            [
                'name' => 'Christmas Day',
                'display' => 0,
                'order' => 0
            ],
            [
                'name' => 'Christmas Day (substitute day)',
                'display' => 0,
                'order' => 0
            ],
            [
                'name' => 'Early May bank holiday',
                'display' => 1,
                'order' => 0
            ],
            [
                'name' => 'Easter Monday',
                'display' => 1,
                'order' => 0
            ],
            [
                'name' => 'Good Friday',
                'display' => 1,
                'order' => 0
            ],
            [
                'name' => 'New Year’s Day',
                'display' => 0,
                'order' => 0
            ],
            [
                'name' => 'New Year’s Day (substitute day)',
                'display' => 0,
                'order' => 0
            ],
            [
                'name' => 'Spring bank holiday',
                'display' => 1,
                'order' => 0
            ],
            [
                'name' => 'Summer bank holiday',
                'display' => 1,
                'order' => 0
            ],
            [
                'name' => 'Summer',
                'display' => 1,
                'order' => 1
            ],
            [
                'name' => 'Teacher Training',
                'display' => 1,
                'order' => 0
            ],
            [
                'name' => 'Platinum Jubilee',
                'display' => 1,
                'order' => 0
            ],
        ]);
    }
}
